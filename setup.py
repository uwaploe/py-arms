# -*- coding: utf-8 -*-
import sys
import os
import imp
from setuptools import setup, find_packages


# Add the current directory to the module search path.
sys.path.insert(0, os.path.abspath('.'))

# Constants
CODE_DIRECTORY = 'pyarms'
DOCS_DIRECTORY = 'docs'
TESTS_DIRECTORY = 'tests'
PYTEST_FLAGS = ['--doctest-modules']

metadata = imp.load_source(
    'metadata', os.path.join(CODE_DIRECTORY, 'metadata.py'))

entry_points = '''
[console_scripts]
readadc=pyarms.apps.readadc:cli
h5merge=pyarms.apps.h5merge:cli
streamadc=pyarms.apps.streamadc:cli
arbgen=pyarms.apps.arbgen:cli
h5extract=pyarms.apps.h5extract:cli
h5expand=pyarms.apps.h5expand:cli
runseq=pyarms.apps.runseq:cli
rostest=pyarms.apps.rostest:cli
readeng=pyarms.apps.readeng:cli
eng=pyarms.apps.eng:cli
engsvc=pyarms.apps.engsvc:cli
runsched=pyarms.apps.runsched:cli
tcmtest=pyarms.apps.tcmtest:cli
readtcm=pyarms.apps.readtcm:cli
'''

entry_points_noext = '''
[console_scripts]
h5merge=pyarms.apps.h5merge:cli
h5extract=pyarms.apps.h5extract:cli
h5expand=pyarms.apps.h5expand:cli
arbgen=pyarms.apps.arbgen:cli
rostest=pyarms.apps.rostest:cli
tcmtest=pyarms.apps.tcmtest:cli
readtcm=pyarms.apps.readtcm:cli
'''

setup_dict = dict(
    name=metadata.package,
    version=metadata.version,
    author=metadata.authors[0],
    author_email=metadata.emails[0],
    maintainer=metadata.authors[0],
    maintainer_email=metadata.emails[0],
    url=metadata.url,
    description=metadata.description,
    packages=find_packages(exclude=('tests', 'builders', 'examples')),
    install_requires=[
        'cffi>=1.0.0',
        'Click',
        'attrs==16.0.0',
        'numpy',
        'scipy',
        'xattr',
        'gcl==0.6.10',
        'pyusb',
        'pyserial',
        'construct==2.5.5',
        'python-dateutil',
        'cmd2',
        'mock',
        'h5py'
    ],
    setup_requires=[
        'cffi>=1.0.0',
        'pkgconfig'
    ],
    # Allow tests to be run with `python setup.py test'.
    tests_require=[
        'pytest>=2.5.1',
        'flake8>=2.1.0',
    ],
    zip_safe=False,  # don't use eggs
    ext_package='pyarms',
    cffi_modules=['builders/build_adc.py:ffi'],
    entry_points=entry_points
)


def main():
    if not sys.platform.startswith('linux'):
        del setup_dict['setup_requires']
        del setup_dict['ext_package']
        del setup_dict['cffi_modules']
        setup_dict['install_requires'].remove('cffi>=1.0.0')
        setup_dict['entry_points'] = entry_points_noext
    cfgdir = os.path.expanduser('~/config')
    if os.path.isdir(cfgdir):
        setup_dict['data_files'] = [(cfgdir, ['tests/arms.cfg',
                                              'tests/ivar.cfg',
                                              'examples/example-seq.cfg',
                                              'examples/example-stream.cfg'])]
    setup(**setup_dict)


if __name__ == '__main__':
    main()
