
typedef unsigned adc_sample_t;

typedef struct {
    adc_sample_t *master;
    adc_sample_t *slave;
} adc_scan_t;

struct adc_clock_s;
typedef struct adc_clock_s adc_clock_t;

struct ADC_s;
typedef struct ADC_s ADC_t;

typedef enum {
    TRIG_START=0,
    TRIG_END=1
} adc_trig_state_t;

typedef enum {
    BOARD_MASTER=0,
    BOARD_SLAVE=1
} adc_board_t;

typedef enum {
    INPUT_DIFF=0,
    INPUT_ZERO=1,
    INPUT_VREF=2
} adc_input_mode_t;

extern "Python" void process_scan(adc_scan_t*, unsigned, unsigned,
                                  unsigned, unsigned, int, void*);
extern "Python" void process_trigger(adc_trig_state_t, char*, void*);

typedef void (*ScanFunc)(adc_scan_t *sp, unsigned m_chans, unsigned m_mask,
                         unsigned s_chans, unsigned s_mask,
                         int n, void* udata);
typedef void (*TrigFunc)(adc_trig_state_t state, char *token, void* udata);

ADC_t* adc_open(const char *master_dev, unsigned master_mask,
                const char *slave_dev, unsigned slave_mask,
                unsigned n_scans);
unsigned adc_get_nchannels(ADC_t *adc, adc_board_t board);
unsigned adc_get_mask(ADC_t *adc, adc_board_t board);
void adc_set_udata(ADC_t *adc, void *udata);
int adc_channels_ready(ADC_t *adc, long timeout_ms);
int adc_sync(ADC_t *adc);

long adc_init(ADC_t *adc, long fsamp, long bufsize, long timeout, float v_peak);
void adc_enable(ADC_t *adc);
void adc_disable(ADC_t *adc);
void adc_close(ADC_t *adc);
int adc_arm(ADC_t *adc, long timeout_ms, int use_hardware);
void adc_trigger(ADC_t *adc);
long adc_read(ADC_t *adc, long n_samples, ScanFunc f, TrigFunc tf);
long adc_read_now(ADC_t *adc, long n_samples, ScanFunc handler);
void adc_vrange(ADC_t *adc, float v_peak);
void adc_input_mode(ADC_t *adc, adc_input_mode_t mode);
