
typedef long long time_t;

typedef struct timespec {
    time_t   tv_sec;
    long     tv_nsec;
    ...;
};

typedef unsigned adc_sample_t;
typedef struct {
    struct timespec     ts;
    int                 scans;
    int                 chans;
    ...;
} adc_data_block_t;


struct adc_clock_s;
typedef struct adc_clock_s adc_clock_t;

struct ADC_s;
typedef struct ADC_s ADC_t;

typedef enum {
    TRIG_START=0,
    TRIG_END=1
} adc_trig_state_t;

typedef enum {
    INPUT_DIFF=0,
    INPUT_ZERO=1,
    INPUT_VREF=2
} adc_input_mode_t;

extern "Python" void process_data(adc_data_block_t*, unsigned, void*);
extern "Python" void process_trigger(adc_trig_state_t, char*, void*);
extern "Python" int process_stream(adc_data_block_t*, unsigned, void*);

typedef void (*DataFunc)(adc_data_block_t *dp, unsigned mask, void *udata);
typedef int (*StreamFunc)(adc_data_block_t *dp, unsigned mask, void *udata);
typedef void (*TrigFunc)(adc_trig_state_t state, char *token, void *udata);

const char* adc_board_type(void);
ADC_t* adc_open(const char *dev, unsigned mask, unsigned scans_per_block);
unsigned adc_get_nchannels(ADC_t *adc);
unsigned adc_get_mask(ADC_t *adc);
void adc_set_udata(ADC_t *adc, void *udata);
int adc_channels_ready(ADC_t *adc, long timeout_ms);
int adc_sync(ADC_t *adc);

long adc_init(ADC_t *adc, long fsamp, long timeout, float v_peak);
void adc_enable(ADC_t *adc);
void adc_disable(ADC_t *adc);
void adc_close(ADC_t *adc);
int adc_arm(ADC_t *adc, long timeout_ms, int use_hardware);
int adc_trigger(ADC_t *adc);
long adc_read(ADC_t *adc, long n_samples, DataFunc f, TrigFunc tf);
void adc_vrange(ADC_t *adc, float v_peak);
void adc_input_mode(ADC_t *adc, adc_input_mode_t mode);
int *adc_get_data(adc_data_block_t *dp);
unsigned long adc_stream(ADC_t *adc, StreamFunc f, TrigFunc tf);
