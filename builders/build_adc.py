import os
import pkgconfig
from cffi import FFI


reqs = 'gsadc'

for req in reqs.split():
    if not pkgconfig.exists(req):
        raise RuntimeError('Required package {0!r} not found'.format(req))

pc_lib = {k: list(v)
          for k, v in pkgconfig.parse(reqs).items()}

ffi = FFI()

headers = """
    #include <gsadc_api.h>

    int *adc_get_data(adc_data_block_t *dp)
    {
        return (int*)&dp->data[0];
    }
"""

ffi.set_source('_adc', headers, **pc_lib)

# Read the API header file
with open(os.path.join(os.path.dirname(__file__), 'gsadc_api.h')) as f:
    ffi.cdef(f.read())


if __name__ == "__main__":
    ffi.compile(verbose=True)
