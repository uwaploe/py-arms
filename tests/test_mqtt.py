#
# Test MQTT pub/sub
#
import time
from Queue import Queue
from subprocess import Popen
from contextlib import contextmanager
from pyarms.mqtt import Messenger


@contextmanager
def mqtt_server():
    proc = Popen(['mosquitto'], close_fds=True)
    time.sleep(3)
    yield proc
    proc.terminate()
    proc.wait()


def test_pubsub():
    mq = Queue()
    with mqtt_server():
        m = Messenger(mq, sub='pytest/#', qos=2)
        assert m.publish('pytest/hello', 'hello world')
        topic, msg = mq.get(block=True, timeout=2)
        assert msg == 'hello world'
        assert m.publish('pytest/hello', 'goodbye', qos=2)
        topic, msg = mq.get(block=True, timeout=2)
        assert msg == 'goodbye'
