import pytest
from pyarms.config import load_engconfig, load_config
from pyarms.engadc import readeng
from io import StringIO
from decimal import Decimal


CAL = u"""
arms = include 'arms.cfg';
"""

DATA = u"""
1483069978.123456 819 819 819 819 0 0 0 0
"""


@pytest.fixture(params=[CAL])
def calfile(request):
    return StringIO(request.param)


@pytest.fixture
def datafile():
    return StringIO(DATA)


def test_readcal(calfile):
    cfg = load_config(calfile)
    fname, cals = load_engconfig(cfg)
    assert fname == '/proc/iguana/adc'
    assert len(cals) == 5
    v, _ = cals[0][2](Decimal(4096)/Decimal(5))
    assert v == Decimal('1.000')
    func = cals[3][2]
    x, units = func(v)
    assert x == (Decimal('191.8') - Decimal('185.8'))
    assert units == 'degC'


def test_readdata(calfile, datafile):
    cfg = load_config(calfile)
    _, cals = load_engconfig(cfg)
    ts, result = readeng(datafile, cals)
    assert len(result) == 4
    name, x, units = result[0]
    assert x == Decimal('11.812')
    assert units == 'volts'


def test_readvolts(calfile, datafile):
    cfg = load_config(calfile)
    _, cals = load_engconfig(cfg)
    ts, result = readeng(datafile, cals, onlyvolts=True)
    assert len(result) == 4
    name, x, units = result[0]
    assert x == Decimal('1.000')
    assert units == 'volts'
