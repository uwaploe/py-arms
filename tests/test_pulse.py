from pyarms.pulse import fmslide, tone


def test_fmslide():
    p = fmslide(12, 30000, 3000, 6000)
    n = int(24 / (0.2 + 0.1)) + 1
    assert len(p) == n
    assert p[0] == 0
    assert p[-1] == 0


def test_tone():
    p = tone(12, 30000, 3000)
    n = int(12 / 0.1) + 1
    assert len(p) == n
    assert p[0] == 0
    assert p[-1] == 0
