#
# Test ADC
#
from pyarms.adc import BaseADC


def test_basic():
    adc = BaseADC([1, 3, 5, 7])
    assert adc.mask == 0xaa
    assert adc.channels == 8
