#
# Test DAC module
#
import pyarms.dac
import mock


def fake_load(seq, **kwds):
    vals = sum([len(block) for block in seq])
    return 2*vals


def test_arbgen():
    with mock.patch('pyarms.dac.USBDA128A') as obj:
        obj.load_waveform.side_effect = fake_load
        ag = pyarms.dac.ArbGen(obj)
        obj.direct.assert_called_with(0, ag.padval)
        ag.fsample = 25000
        obj.set_clock.assert_called_with(25000)
        ag._fsample = 25000
        ag.vpeak = 5
        obj.set_vrange.assert_called_with(pyarms.dac.DAC_RANGE_5V)
        ag._vpeak = 5.
        ag.add_waveform('sig1', 4., [0.]*100)
        assert (ag.waveforms['sig1'].data[-1][-1] & pyarms.dac.DD_EOM) != 0
        ag.add_waveform('sig2', 4., [0.]*200, fsample=20000)
        ag.arm('sig1', timeout=3000)
        obj.reset.assert_called_with()
        obj.set_clock.assert_called_with(25000)
        obj.load_waveform.assert_called_with(ag.waveforms['sig1'].data,
                                             timeout=3000)
        obj.enable.assert_not_called()
        ag.arm('sig2', timeout=3000)
        obj.set_clock.assert_called_with(20000)
