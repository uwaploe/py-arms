import pytest
from pyarms.config import parse_sequence, load_config, \
    parse_schedule, sequence_duration
from io import StringIO


CFG = u"""arms = include 'arms.cfg' { bufsize = 10000; };
steps = arms.Range {
  start = 0.0;
  count = 40;
  inc = 2.0;
};
sample = {
  pings = 1;
  xmtr = arms.Arbgen { enable = false; waveform = 'test.arb'; };
  rcvr = arms.ADC;
};
"""

BADCFG = u"""arms = include 'arms.cfg' { bufsize = 10000; };
steps = arms.Range {
  start = 0.0;
  count = 40;
  inc = 2.0;
};
sample = {
  pings = 1;
  xmtr = arms.Arbgen { enable = true; };
  rcvr = arms.ADC;
};
"""

SCHED = u"""name,interval,reverse,count,timeout
test.cfg,02:00,yes,2,02:00
long.cfg,06:00,yes,10,
"""


@pytest.fixture
def schedfile():
    return StringIO(SCHED)


@pytest.fixture
def cfgfile():
    return StringIO(CFG)


@pytest.fixture
def bad_cfgfile():
    return StringIO(BADCFG)


@pytest.fixture
def waveform():
    return [0] * 1000


def test_seq(cfgfile):
    steps, settings = parse_sequence(load_config(cfgfile))
    assert 'wavedir' in settings
    assert 'bufsize' in settings
    assert settings['bufsize'] == 10000
    assert len(steps) == 40
    assert steps[-1].angle == 78
    sample = steps[0].sample
    assert sample.delay == 0
    assert sample.pings == 1
    assert sample.rcvr.channels == list(range(10))
    assert sample.xmtr.gate > 0
    assert sample.duration == sample.rcvr.window


def test_nowaveform(bad_cfgfile):
    cfg = load_config(bad_cfgfile)
    with pytest.raises(ValueError):
        steps, settings = parse_sequence(cfg)


def test_revseq(cfgfile):
    steps, settings = parse_sequence(load_config(cfgfile), rev=True)
    assert 'wavedir' in settings
    assert 'bufsize' in settings
    assert settings['bufsize'] == 10000
    assert len(steps) == 40
    assert steps[0].angle == 78
    assert steps[-1].angle == 0
    sample = steps[0].sample
    assert sample.delay == 0
    assert sample.pings == 1
    assert sample.rcvr.channels == list(range(10))


def test_waveform(cfgfile, waveform):
    steps, settings = parse_sequence(load_config(cfgfile))
    sample = steps[0].sample
    sample.xmtr.data = waveform
    assert sample.xmtr.pulselen == int(len(waveform) * 1e6 / sample.xmtr.fsample)


def test_sched(schedfile):
    sched = list(parse_schedule(schedfile))
    assert len(sched) == 2


def test_duration(cfgfile):
    steps, settings = parse_sequence(load_config(cfgfile))
    assert sequence_duration(steps) == (1600 + 78)
