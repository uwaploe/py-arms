import pytest
from pyarms.config import load_rotator, load_config
from io import StringIO


CFG = u"""Rotator = {
    device = '/dev/ttyS2';
    baudrate = 9600;
    settings = {
        ccw = 45;
        cw = 315;
        maxvel = 5;
        accel = 2;
        brake = 80;
    };
};
"""

CFG2 = u"""
arms = include 'arms.cfg';
Rotator = arms.Rotator {
    settings = base.settings {
        accel = 6;
        brake = 128;
        park = 40;
    };
};
"""


@pytest.fixture(params=[CFG, CFG2])
def cfgfile(request):
    return StringIO(request.param)


def test_readcfg(cfgfile):
    cfg = load_config(cfgfile)
    desc = load_rotator(cfg)
    assert desc.device == '/dev/ttyS2'
    if desc.settings['accel'] == 6:
        assert desc.settings['brake'] == 128
        assert desc.settings['park'] == 40
    else:
        assert desc.settings['brake'] == 80
