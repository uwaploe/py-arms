#
# Test various utility functions
#
from pyarms.config import parse_duration


def calc_window(fs, val):
    return int(fs * parse_duration(val) / 1000000)


def test_duration():
    fs = 20000
    assert calc_window(fs, '1s') == fs
    assert calc_window(fs, '1') == fs
    assert calc_window(fs, '100ms') == fs//10
    assert calc_window(fs, '1m10s') == (70 * fs)
    assert calc_window(fs, '1m20') == (80 * fs)
