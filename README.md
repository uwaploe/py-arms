# Python Package for ARMS

This package contains the primary data-acquisition and control software
for the Acoustic Reverberation Measurement System (ARMS), it can also be
used on related projects such as IVAR.

## Installation

Install using ``pip`` in a virtual environment under the *sysop* account

``` shellsession
virtualenv ~/venv
source ~/venv/bin/activate
pip install pyarms-$VERSION.tar.gz
```

## Applications

### readadc

``` shellsession
Usage: readadc [OPTIONS] FSAMPLE DURATION OUTPUT

  Sample A/D input at FSAMPLE hz for the specified DURATION. The DURATION
  value is compatible with the Golang duration string format; a string of
  decimal numbers, each with an optional fraction and a unit suffix, such as
  '500ms', '1.2s', or '1m10s'.

  OUTPUT is an HDF5-format file which will contain all of the meta-data. The
  ping data will be written to a series of binary files named
  'ping_NNNNNN.bin' in the same directory as OUTPUT. The ping data can be
  incorporated into the HDF5 file using the 'h5merge' program.

Options:
  --hwtrigger / --no-hwtrigger  use hardware trigger
  --chans TEXT                  input channel count
  --vpeak FLOAT                 peak input voltage
  --bufsize INTEGER             scans to buffer
  --pings INTEGER               number of pings
  --help                        Show this message and exit.
```

### arbgen

``` shellsession
Usage: arbgen [OPTIONS] NCYCLES FS F0

  Generate and transmit a waveform of length NCYCLES at frequency F0 through
  ARMS D/A board. The sampling frequency is FS.

  If '--f1' is given, the transmitted waveform will be an FM sweep with
  starting frequency F0 and ending frequency F1. In this case, NCYCLES will
  be measured at the average frequency.

Options:
  --vmax FLOAT                  peak waveform voltage
  --hwtrigger / --no-hwtrigger  use hardware trigger
  --taper FLOAT                 waveform taper fraction
  --f1 FLOAT                    ending frequency for sweeps
  -o, --output FILE             save waveform to FILE
  --raw FILE                    save raw waveform values to FILE
  --help                        Show this message and exit.
```

### h5merge

``` shellsession
Usage: h5merge [OPTIONS] H5FILE

Options:
  --compress / --no-compress  compress ping data
  --clean / --no-clean        remove ping files after merging
  --group TEXT                name of HDF5 group containing ping data
  --help                      Show this message and exit.
```

### h5expand

``` shellsession
Usage: h5expand [OPTIONS] H5FILE OUTDIR

  Extract the contents of an HDF5 data file into a directory tree rooted at
  OUTDIR. Each group is a subdirectory and each data-set is a separate file.

Options:
  --volts / --no-volts  write A/D values as volts rather than counts
  --help                Show this message and exit.
```

### streamadc

``` shellsession
Usage: streamadc [OPTIONS] CFGFILE OUTPUT

  Stream A/D data to a series of HDF5 files in the directory OUTPUT. The
  sampling parameters are defined in CFGFILE.

  To stop the data collection, send a 'halt' message via MQTT as follows:

    mosquitto_pub -t adc/commands -m halt

Options:
  -b, --bufsize INTEGER  A/D buffer size in scans
  --help                 Show this message and exit.
```

### runseq

``` shellsession
Usage: runseq [OPTIONS] CFGFILE OUTDIR

  Run an ARMS data collection sequence. A sequence consists of one or more
  acoustic pings transmitted and received at one or more Rotator steps.
  CFGFILE is the configuration file which describes the sequence, OUTDIR is
  the data directory which will contain the output files.

Options:
  --rev / --no-rev       also run the reverse sequence
  -b, --bufsize INTEGER  A/D buffer size in scans
  --help                 Show this message and exit.
```

### readeng

``` shellsession
Usage: readeng [OPTIONS] CFGFILE INTERVAL OUTFILE

  Sample the engineering A/D every INTERVAL seconds and write the data to
  OUTFILE in a format suitable for merging into an HDF5 data file. CFGFILE
  contains the A/D calibration values.

Options:
  --help  Show this message and exit.
```

### rostest

``` shellsession
Usage: rostest [OPTIONS] DEVICE [COMMANDS]...

  Interactive test program for an ROS Rotator. DEVICE is the serial port
  that the Rotator is connected to.

Options:
  -b, --baud INTEGER  set serial baud rate
  --help              Show this message and exit.
```
