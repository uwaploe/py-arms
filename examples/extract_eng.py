#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import h5py
import numpy as np


@click.command()
@click.argument('outfile')
@click.argument('h5file', type=click.Path(exists=True), nargs=-1)
def cli(outfile, h5file):
    """
    Extract the engineering data from a series of ARMS HDF5
    data files.
    """
    rows = []
    for fn in h5file:
        row = []
        with h5py.File(fn) as f:
            # Use the timestamp of the first ping as the
            # timestamp for the engineering data. Convert
            # to 1-second resolution.
            t = f['/pings/ping_000001/timestamp'][()]
            row.append(('time', t.dtype, int(t/1000000)))
            # Extract the name, data-type, and value for each
            # engineering data set.
            for dset in f['/metadata/eng'].values():
                row.append((dset.name.split('/')[-1],
                            dset.dtype,
                            dset[()]))
        rows.append(row)
    # Separate the data types and values.
    dtype = [(str(e[0]), e[1]) for e in rows[0]]
    data = [tuple([e[2] for e in r]) for r in rows]
    # Create a record array
    rec = np.rec.array(data, dtype=dtype)
    # Convert the list of names into a file header line
    header = ' '.join([d[0] for d in dtype])
    # Printf format string for each column, all floating point
    # except the timestamp.
    fmts = ['%d'] + ['%.3f'] * (len(dtype) - 1)
    # Write the file.
    np.savetxt(outfile, rec, fmt=fmts, delimiter=' ', header=header)


if __name__ == '__main__':
    cli()
