#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import h5py
import numpy as np
import scipy.signal


@click.command()
@click.option('--dgroup', default='/pings',
              help='name of HDF5 group with A/D data')
@click.argument('h5file', type=click.Path(exists=True))
def cli(dgroup, h5file):
    """
    Calculate the RMS input voltage for all received signals
    in an ARMS HDF5 data file.
    """
    if not dgroup.startswith('/'):
        dgroup = '/' + dgroup

    with h5py.File(h5file, 'r') as f:
        vscale = f['/metadata/rcvr/vscale'][()]
        index = list(enumerate(f['/metadata/rcvr/channels']))

        for grp in f[dgroup].values():
            rms = []
            for i, c in index:
                x = scipy.signal.detrend(grp['signal'][:, i] * vscale,
                                         type='constant')
                rms.append((c, np.std(x)))
            click.secho(grp.name + ': ', fg='yellow', nl=False)
            for chan, vrms in rms:
                click.echo('[{:d}]='.format(chan), nl=False)
                click.secho('{:.3f}mV '.format(vrms*1000), fg='green',
                            nl=False)
            click.echo('', nl=True)


if __name__ == '__main__':
    cli()
