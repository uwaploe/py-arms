#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import h5py
import numpy as np
import scipy.signal


def analytic(x):
    n = len(x)
    xp = np.fft.fft(x)
    s = np.ones(n) * 2
    s[n/2:] = 0
    return np.fft.ifft(xp * s)


@click.command()
@click.option('-o', '--outfile', type=click.File('wb'),
              help='output file')
@click.argument('h5file', type=click.Path(exists=True))
@click.argument('channel', type=int)
def cli(outfile, h5file, channel):
    """
    Calculate the relative time offsets for a series of pings.
    """
    cols = []
    with h5py.File(h5file, 'r') as f:
        vscale = f['/metadata/rcvr/vscale'][()]
        xfs = f['/metadata/xmtr/fsample'][()]
        rfs = f['/metadata/rcvr/fsample'][()]
        index = dict([(c, i)
                      for i, c in enumerate(f['/metadata/rcvr/channels'])])
        try:
            i = index[channel]
        except KeyError:
            click.secho('Invalid channel', fg='red')
            raise SystemExit(1)

        path = '/metadata/xmtr/signal'
        nx = f[path].len()
        xdt = 1. / xfs
        xt = np.linspace(0., (nx - 1)*xdt, nx, endpoint=True)
        rt = np.linspace(0., (nx - 1)*xdt, int(nx * rfs / xfs), endpoint=True)
        replica = np.interp(rt, xt, f[path][:], left=0, right=0) * 0.7
        taus = []
        for grp in f['/pings'].values():
            x = scipy.signal.detrend(grp['signal'][:, i] * vscale,
                                     type='constant')
            y = np.correlate(x, replica)
            cols.append(x)
            taus.append(np.argmax(y) * 1000. / rfs)

    if outfile:
        block = np.stack(tuple(cols), axis=1)
        np.savetxt(outfile, block)
    for i, tau in enumerate(taus):
        click.secho('Ping {:d}: tau = {:.1f}'.format(i+1, tau),
                    fg='green')


if __name__ == '__main__':
    cli()
