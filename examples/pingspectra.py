#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import h5py
import numpy as np
import scipy.signal


@click.command()
@click.option('--start', default=None,
              help='starting sample index')
@click.option('--stop', default=None,
              help='ending sample index')
@click.option('--psd/--no-psd', default=False,
              help='Calculate power spectral density')
@click.argument('h5file', type=click.Path(exists=True))
@click.argument('channel', type=int)
@click.argument('outfile', type=click.File('wb'))
def cli(start, stop, psd, h5file, channel, outfile):
    """
    Calculate the power spectra for all received signals on a
    given CHANNEL. The output is written to OUTFILE in ASCII with
    one column for each "ping", the first column has the frequency
    bin values.
    """
    if start is not None:
        start = int(start)
    if stop is not None:
        stop = int(stop)
    sel = slice(start, stop)

    cols = []
    with h5py.File(h5file, 'r') as f:
        vscale = f['/metadata/rcvr/vscale'][()]
        rfs = f['/metadata/rcvr/fsample'][()]
        index = dict([(c, i)
                      for i, c in enumerate(f['/metadata/rcvr/channels'])])
        try:
            i = index[channel]
        except KeyError:
            click.secho('Invalid channel', fg='red')
            raise SystemExit(1)

        scaling = 'density' if psd else 'spectrum'
        for grp in f['/pings'].values():
            x = scipy.signal.detrend(grp['signal'][sel, i] * vscale,
                                     type='constant')
            freqs, sx = scipy.signal.welch(x, rfs, nperseg=1024,
                                           scaling=scaling)
            cols.append(sx)

    if cols:
        cols.insert(0, freqs)
        block = np.stack(tuple(cols), axis=1)
        np.savetxt(outfile, block)
    else:
        click.secho('No data found', fg='red')


if __name__ == '__main__':
    cli()
