# -*- coding: utf-8 -*-
"""
.. module:: pyarms.config
     :platform: Linux
     :synopsis: interface to the various ARMS config files.
"""
import attr
import re
import os
import gcl
from gcl import query
from decimal import Decimal
from functools import partial
from . import engadc

# Adapted from: https://github.com/icholy/Duration.py/blob/master/duration.py
_microsecond_size = 1
_millisecond_size = 1000 * _microsecond_size
_second_size = 1000 * _millisecond_size
_minute_size = 60 * _second_size
_hour_size = 60 * _minute_size

units = {
    "us": _microsecond_size,
    "µs": _microsecond_size,
    "μs": _microsecond_size,
    "ms": _millisecond_size,
    "s": _second_size,
    "m": _minute_size,
    "h": _hour_size,
    "": _second_size
}


def parse_duration(duration):
    """
    Convert a Golang-style duration string to microseconds.
    """
    pat = re.compile(r'([\-\+\d\.]+)([a-zµμ]*)')
    total = 0
    matches = pat.findall(duration)
    if not len(matches):
        return 0
    for value, unit in matches:
        if unit not in units:
            raise ValueError('Unknown unit {} in {}'.format(unit, duration))
        try:
            total += float(value) * units[unit]
        except Exception:
            raise ValueError('Invalid value {} in {}'.format(value, duration))
    return total


def load_waveform(infile):
    """
    Read an Arbgen waveform file. A waveform file is simply a
    list of ASCII floating-point values, one value per line.
    Lines starting with '#' are treated as comments.

    :param infile: input file object
    :returns: list of float-point numbers
    """
    wave = []
    for line in infile:
        if not line.startswith(b'#'):
            wave.append(float(line.strip()))
    return wave


@attr.s
class Xmtr(object):
    """
    Transmitter configuration object.
    """
    fsample = attr.ib(convert=int)
    vmax = attr.ib(convert=float)
    waveform = attr.ib(default=None)
    data = attr.ib(default=attr.Factory(list), repr=False)
    wavedir = attr.ib(default=None)
    enable = attr.ib(default=True)
    gate = attr.ib(default=-1)

    @property
    def pulselen(self):
        """
        Waveform length in microseconds.
        """
        return (len(self.data) * 1000000) / self.fsample


@attr.s
class Rcvr(object):
    """
    Receiver configuration object.
    """
    fsample = attr.ib(convert=int)
    vmax = attr.ib(convert=float)
    window = attr.ib(convert=parse_duration)
    channels = attr.ib(validator=attr.validators.instance_of(list))
    enable = attr.ib(default=True)

    @property
    def nsamples(self):
        return int(self.window * self.fsample / 1000000)


@attr.s
class Sample(object):
    """
    Sample configuration object.
    """
    xmtr = attr.ib(validator=attr.validators.instance_of(Xmtr))
    rcvr = attr.ib(validator=attr.validators.instance_of(Rcvr))
    delay = attr.ib(default='0', convert=parse_duration)
    pings = attr.ib(default='1', convert=int)

    @property
    def duration(self):
        """
        Total sample duration in microseconds.
        """
        return int(self.pings * self.rcvr.window)


@attr.s
class Step(object):
    """
    Sequence-step configuration object.
    """
    angle = attr.ib()
    sample = attr.ib(validator=attr.validators.instance_of(Sample))


@attr.s
class SerialDev(object):
    """
    Rotator configuration.
    """
    device = attr.ib()
    baudrate = attr.ib(convert=int)
    settings = attr.ib(default=attr.Factory(dict))


def expand_range(rng):
    xi = range(rng['count'])
    return [rng['start'] + x * rng['inc'] for x in xi]


def load_config(infile, path=None):
    """
    Read a GCL based configuration file.

    :param infile: input file object
    :param path: directory path to search for "include" files.
    """
    env = {
        'range': range,
        'decimal': Decimal,
        'dec': Decimal
    }
    if path:
        cfg = gcl.loads(infile.read(),
                        env=env,
                        loader=gcl.loader_with_search_path(path))
    else:
        cfg = gcl.loads(infile.read(), env=env)
    return cfg


def parse_sequence(obj, rev=False):
    """
    Parse a sequence description from configuration data.

    :param obj: configuration object from :func:`load_config`
    :param rev: if ``True``, reverse the order of the rotator steps.
    :returns: list of :class:`Step` instances, global settings dictionary.
    :rtype: tuple
    """
    if not isinstance(obj['steps'], list):
        angles = expand_range(obj['steps'])
    else:
        angles = obj['steps']
    if rev:
        angles.reverse()
    steps = []
    xmtr = Xmtr(**(gcl.util.to_python(obj['sample']['xmtr'])))
    xmtr.wavedir = obj['arms']['wavedir']
    if xmtr.enable:
        if not xmtr.waveform:
            raise ValueError('Waveform file not specified')
        filename = os.path.join(obj['arms']['wavedir'],
                                xmtr.waveform)
        xmtr.waveform = os.path.realpath(filename)
        with open(filename, 'rb') as f:
            xmtr.data = load_waveform(f)
    rcvr = Rcvr(**(gcl.util.to_python(obj['sample']['rcvr'])))
    for angle in angles:
        step = Step(angle=angle,
                    sample=Sample(xmtr=xmtr,
                                  rcvr=rcvr,
                                  pings=obj['sample']['pings']))
        steps.append(step)
    settings = {k: v for k, v in obj['arms'].items()
                if ('a' <= k[0] <= 'z')}
    return steps, settings


def sequence_duration(steps, speed=1):
    """
    Estimate the total time required to run a sequence.

    :param steps: list of steps returned by :func:`parse_sequence`.
    :param speed: rotator speed in deg/s
    :return: time estimate in seconds
    """
    tsample = sum([s.sample.duration for s in steps])
    angles = [s.angle for s in steps if s.angle >= 0]
    if angles:
        distance = sum([abs(a - b) for a, b in zip(angles[1:], angles[:-1])])
    else:
        distance = 0
    return (tsample / 1000000.) + (distance / speed)


def load_engconfig(cfg):
    """
    Load the configuration information for the engineering A/D.

    :param obj: configuration object from :func:`load_config`
    :return: A/D file name, list of tuples (chan, name, calibration function)
    """
    def fquant(func, prec, x):
        """
        Return the value of func(x) quantized to the specified
        Decimal precision.
        """
        y, units = func(x)
        return y.quantize(prec), units
    q = query.GPath(['Engadc', '*.Engadc'])
    results = q.select(cfg)
    try:
        eng = list(results.values())[0]
    except IndexError:
        raise ValueError('Calibration data not found')
    prec = Decimal(eng['vprec'])
    vcvt = partial(fquant,
                   engadc.Calibration([Decimal(0),
                                       Decimal(eng['vscale'])],
                                      'volts'),
                   prec)
    r = [(-1, '', vcvt)]
    for entry in eng['cals']:
        if 'prec' in entry:
            prec = Decimal(entry['prec'])
            cvt = partial(fquant,
                          engadc.Calibration([Decimal(x) for x in entry['c']],
                                             entry['units']),
                          prec)
            r.append([entry['channel'],
                      entry['name'],
                      cvt])
        else:
            r.append([entry['channel'],
                      entry['name'],
                      engadc.Calibration([Decimal(x) for x in entry['c']],
                                         entry['units'])])
    return eng['file'], r


def load_serialdev(cfg, gpath):
    """
    Load the configuration information for a serial device

    :param obj: configuration object from :func:`load_config`
    :param gpath: GPath for device configuration
    :return: :class:`SerialDev` instance
    """
    q = query.GPath(gpath)
    results = q.select(cfg)
    try:
        obj = list(results.values())[0]
    except IndexError:
        raise ValueError('Device configuration not found')
    desc = SerialDev(device=obj['device'], baudrate=obj['baudrate'])
    desc.settings = gcl.util.to_python(obj['settings'])
    return desc


def load_rotator(cfg):
    """
    Load the configuration information for the Rotator

    :param obj: configuration object from :func:`load_config`
    :return: :class:`SerialDev` instance
    """
    return load_serialdev(cfg, ['Rotator', '*.Rotator'])


def load_attitude(cfg):
    """
    Load the configuration information for the Attitude Sensor.

    :param obj: configuration object from :func:`load_config`
    :return: :class:`SerialDev` instance
    """
    return load_serialdev(cfg, ['Attitude', '*.Attitude'])


def write_object(outfile, obj):
    outfile.write('{')
    for k, v in obj:
        outfile.write('{} = {!r};'.format(k, v))
    outfile.write('}')


def parse_schedule(infile):
    """
    Generator to parse a CSV schedule file. Returns a dictionary
    for each line.
    """
    cols = ('name', 'interval', 'reverse', 'count', 'timeout')
    n = len(cols)
    marker = cols[0] + ','
    linenum = 1
    for line in infile:
        if not (line.startswith('#') or line.startswith(marker)):
            fields = line.strip().split(',')
            if len(fields) != n:
                raise TypeError('Error on line {:d}'.format(linenum))
            yield dict(zip(cols, fields))
        linenum += 1


def schedule_to_gcl(infile, outfile):
    """
    Convert a CSV-format schedule file to GCL.
    """
    revmap = {
        '0': 'false',
        '1': 'true',
        'yes': 'true',
        'no': 'false'
    }
    includes = {}
    objs = []
    for d in parse_schedule(infile):
        obj = []
        seq, _ = os.path.splitext(d['name'])
        includes[seq] = d['name']
        obj.append(('name', seq))
        obj.append(('interval', d['interval']))
        obj.append(('timeout', d['timeout'] or '00:00'))
        obj.append(('reverse', revmap[d['reverse']]))
        count = d.get('count', '1')
        obj.append(('count', int(count)))
        objs.append(obj)
    for k, v in includes.items():
        outfile.write('{} = include {!r};\n'.format(k, v))
    outfile.write('schedule = [')
    for i, obj in enumerate(objs):
        if i > 0:
            outfile.write(',')
        outfile.write('\n')
        write_object(outfile, obj)
    outfile.write('\n];\n')
