# -*- coding: utf-8 -*-
"""
.. module:: pyarms.pulses
     :platform: Linux
     :synopsis: waveform generation functions
"""
import math
import numpy as np
from scipy.signal import chirp


def apply_taper(data, taper_frac):
    npoints = len(data)
    n = int(taper_frac * npoints)
    omega = math.pi / (2. * n)
    head = np.sin(np.arange(n, dtype=np.float32) * omega)
    tail = np.sin(np.arange(n-1, -1, -1, dtype=np.float32) * omega)
    scale = np.concatenate((head,
                            np.ones((npoints - 2*n,), dtype=np.float32),
                            tail))
    return data * scale


def fmslide(ncycles, fs, f0, f1, taper=0):
    """
    Generate an FM slide (sweep) from starting frequency *f0* to
    ending frequency *f1*.

       0 < f0 < f1 <= fs/2
       0 <= taper <= 0.5

    :param npoints: pulse length in cycles
    :param fs: sampling frequency
    :param f0: starting frequency
    :param f1: ending frequency
    :param taper: fractional taper length
    """
    # Normalize frequencies
    f0 = f0 / float(fs)
    f1 = f1 / float(fs)
    npoints = int((ncycles * 2) / (f0 + f1)) + 1
    t = np.arange(npoints, dtype=np.float32)
    p = chirp(t, f0, t[-1], f1, 'linear', -90.)
    p[0] = 0
    p[-1] = 0.
    if taper > 0:
        p = apply_taper(p, taper)
    return p


def tone(ncycles, fs, f0, taper=0):
    """
    Generate a single frequency tone.

      0 < f0 <= fs/2

    :param npoints: pulse length in cycles
    :param fs: sampling frequency
    :param f0: tone frequency
    :param taper: fractional taper length
    """
    # Normalize frequency
    f0 = f0 / float(fs)
    npoints = int(ncycles / f0) + 1
    omega = 2. * math.pi * f0
    p = np.sin(np.arange(npoints, dtype=np.float32) * omega)
    p[-1] = 0.
    if taper > 0:
        p = apply_taper(p, taper)
    return p
