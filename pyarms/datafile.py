# -*- coding: utf-8 -*-
"""
.. module:: pyarms.datafile
     :platform: Linux
     :synopsis: create ARMS data files
"""
import time


class File(object):
    """
    A wrapper around a :class:`h5py.File` instance to manage
    the ARMS specific format.
    """
    data_group = 'pings'
    data_prefix = 'ping'

    def __init__(self, hf, **kwds):
        """
        Instance initializer.

        :param hf: h5py.File object
        :param kwds: optional file attributes and values
        """
        self.f = hf
        self.pings = 0
        mg = self.f.create_group('metadata')
        for name in ('rcvr', 'eng'):
            mg.create_group(name)
        self.dg = self.f.create_group(self.data_group)
        for k, v in kwds.items():
            self.f.attrs[k] = v
        self.f.attrs['created'] = int(time.time() * 1000000)

    def __len__(self):
        return self.pings

    def __getattr__(self, name):
        return getattr(self.f, name)

    def add_receiver(self, cfg):
        """
        Add receiver configuration metadata.

        :param cfg: receiver configuration
        :type cfg: :class:`pyarms.config.Rcvr`
        """
        nchans = len(cfg.channels)
        vscale = 2. * cfg.vmax / (1 << 24)
        g = self.f['/metadata/rcvr']
        g.create_dataset('channels',
                         (nchans,),
                         data=cfg.channels,
                         dtype='i4')
        g.create_dataset('vscale',
                         (),
                         data=vscale,
                         dtype='f').attrs['units'] = 'volts/count'
        g.create_dataset('fsample',
                         (),
                         data=cfg.fsample,
                         dtype='i4').attrs['units'] = 'Hz'
        return g

    def add_transmitter(self, cfg):
        """
        Add transmitter configuration metadata.

        :param cfg: transmitter configuration
        :type cfg: :class:`pyarms.config.Xmtr`
        """
        g = self.f.get('/metadata/xmtr',
                       self.f.create_group('/metadata/xmtr'))
        g.attrs['filename'] = cfg.waveform
        g.create_dataset('vmax',
                         (),
                         data=cfg.vmax,
                         dtype='f').attrs['units'] = 'volts'
        g.create_dataset('fsample',
                         (),
                         data=cfg.fsample,
                         dtype='i4').attrs['units'] = 'Hz'
        if cfg.data:
            g.create_dataset('signal',
                             (len(cfg.data),),
                             data=cfg.data,
                             compression='gzip',
                             dtype='f4')

    def add_eng(self, data):
        """
        Add engineering data-sets. Each element of *data* is
        a tuple of:

            NAME, VALUE, DTYPE, ATTRIBUTES
        """
        g = self.f['/metadata/eng']
        for name, value, dtype, attrs in data:
            dims = (len(value),) if isinstance(value, list) else ()
            ds = g.create_dataset(name, dims, data=value, dtype=dtype)
            for k, v in attrs.iteritems():
                ds.attrs[k] = v

    def next_ping(self):
        return '{}_{:06d}'.format(self.data_prefix, self.pings + 1)

    def add_ping(self, ts):
        """
        Add a ping group.

        :param ts: ping timestamp.
        """
        name = self.next_ping()
        self.pings += 1
        g = self.dg.create_group(name)
        g.create_dataset('timestamp',
                         (),
                         data=int(ts * 1000000),
                         dtype='i8').attrs['units'] = 'microseconds'
        return g
