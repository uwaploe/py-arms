# -*- coding: utf-8 -*-
"""
.. module:: pyarms.dac
     :platform: Linux
     :synopsis: interface to the USB DAC board.
"""
from array import array
import usb.core
import usb.util as uu


USB_WRITE = uu.build_request_type(uu.CTRL_OUT,
                                  uu.CTRL_TYPE_VENDOR,
                                  uu.CTRL_RECIPIENT_DEVICE)

# Voltage ranges
DAC_RANGE_2_5V = 0
DAC_RANGE_5V = 1
DAC_RANGE_10V = 3

# DAC Data bits
DD_EOM = (1 << 15)
DD_EOF = (1 << 14)
DD_EOD = (1 << 13)
DD_LOOP = (1 << 12)


class Wave(object):
    """
    Waveform to be uploaded to the DAC board. The waveform is split
    into chunks of size :ivar:`blocksize` before being transferred.
    """
    blocksize = 31*512

    def __init__(self, fs, data):
        self.fs = fs
        n = len(data)
        blocks, rem = divmod(n, self.blocksize)
        self.data = []
        self.samples = n
        p = 0
        for i in range(blocks):
            q = p + self.blocksize
            self.data.append(data[p:q])
            p += self.blocksize
        self.data.append(data[p:])

    def __len__(self):
        return self.samples

    def asarray(self):
        d = []
        for block in self.data:
            for b in block:
                d.append(b & 0xfff)
                if b & DD_EOM:
                    return d
        return d

    def dump(self, outf):
        """
        Write the waveform values to a file. Two space separated
        columns are output. The first column contains the control
        bits and the second column is the output value [0, 4095].

        :param outf: output file object.
        """
        for block in self.data:
            for b in block:
                outf.write('{:d} {:d}\n'.format((b & 0xf000) >> 12, b & 0xfff))
                if b & DD_EOM:
                    return


class USBDA128A(object):
    """
    Interface to the ACCES I/O USB-DA12-8A analog output (DAC) board.

    :ivar fref: clock reference frequency
    :ivar reqs: USB bRequest codes for the board
    """
    fref = 12000000
    channels = 8
    reqs = {
        'DAC_CONTROL': 0xb0,
        'DAC_DATAPTR': 0xb1,
        'DAC_DIVISOR': 0xb2,
        'DAC_DIRECT': 0xb3,
        'DAC_RANGE': 0xb7,
        'SET_STATUS': 0xcc
    }

    def __init__(self, product_id=0x4002):
        """
        Instance initializer. Finds the first instance of the board on
        the USB bus.

        :raise RuntimeError: if no device is found.
        """
        self.dev = usb.core.find(idVendor=0x1605, idProduct=product_id)
        if self.dev is None:
            raise RuntimeError('Cannot find USB device')
        self.dev.set_configuration()
        cfg = self.dev.get_active_configuration()
        intf = cfg[(0, 0)]
        self.ep = intf[0]

    def set_clock(self, freq):
        """
        Set the output sampling frequency.

        :param freq: desired frequency in Hz
        :return: actual sampling frequency (may differ from *freq*)
        """
        divisor = int(self.fref / freq)
        freq = self.fref // divisor
        self.dev.ctrl_transfer(USB_WRITE,
                               self.reqs['DAC_DIVISOR'],
                               0,
                               divisor,
                               None)
        return freq

    def set_status(self, value):
        """
        Set the STATUS output line.

        :param value: ``True`` or ``False``
        """
        x = 1 if value else 0
        self.dev.ctrl_transfer(USB_WRITE,
                               self.reqs['SET_STATUS'],
                               0,
                               x,
                               None)

    def set_vrange(self, code):
        """
        Set the output voltage range. This is actually a no-op for the
        current revision of the board (the voltage range is set by a
        jumper) but the command is supported for a future enhancement.
        """
        pass

    def reset(self):
        """
        Reset the board. Should be called before uploading a new waveform.
        """
        self.dev.ctrl_transfer(USB_WRITE,
                               self.reqs['DAC_CONTROL'],
                               0x80,
                               0,
                               None)

    def enable(self):
        """
        Enable the output.

        Note that for waveforms with more than 96k samples, the board
        firmware enables the output automatically (!).
        """
        self.dev.ctrl_transfer(USB_WRITE,
                               self.reqs['DAC_CONTROL'],
                               0x01,
                               0,
                               None)

    def load_waveform(self, blocks, timeout=None):
        """
        Upload a new waveform. See page 8 of the board manual for a
        description of the data format.
        """
        self.dev.ctrl_transfer(USB_WRITE,
                               self.reqs['DAC_DATAPTR'],
                               0,
                               0,
                               None)
        sent = 0
        for b in blocks:
            sent += self.dev.write(self.ep, b, timeout=timeout)
        return sent

    def direct(self, channel, value):
        """
        Set the output value of a DAC channel.
        """
        self.dev.ctrl_transfer(USB_WRITE,
                               self.reqs['DAC_DIRECT'],
                               value,
                               channel,
                               None)


class ArbGen(object):
    """
    Arbitrary Waveform Generator based on USBDA128A.

    :ivar maxlen: maximum waveform length (samples)
    :ivar minlen: minimum waveform length (samples)
    :ivar padval: value to use to pad waveform out to *minlen*
    """
    maxlen = 128*1024
    minlen = 65*1024
    padval = 2047

    def __init__(self, dev):
        """
        Instance initializer.

        :param dev: USBDA128A object
        """
        self.dev = dev
        self._fsample = 0
        self._vpeak = 0
        self.waveforms = {}
        for chan in range(self.dev.channels):
            self.dev.direct(chan, self.padval)

    def __contains__(self, name):
        return name in self.waveforms

    def __getitem__(self, name):
        return self.waveforms[name]

    @property
    def fsample(self):
        """Default sampling frequency."""
        return self._fsample

    @fsample.setter
    def fsample(self, value):
        self._fsample = self.dev.set_clock(value)

    @property
    def vpeak(self):
        """Peak output voltage."""
        return self._vpeak

    @vpeak.setter
    def vpeak(self, value):
        vrange = DAC_RANGE_2_5V
        if value > 5.:
            vrange = DAC_RANGE_10V
            self._vpeak = 10.
        else:
            vrange = DAC_RANGE_5V
            self._vpeak = 5.
        self.dev.set_vrange(vrange)

    def pause(self):
        """
        Force PAUSE line (J4-6) low to inhibit waveform output. The
        STATUS line (J4-3) is connected to the PAUSE line.
        """
        self.dev.set_status(False)

    def unpause(self):
        """
        Force PAUSE line high to enable waveform output.
        """
        self.dev.set_status(True)

    def add_waveform(self, name, vmax, data, fsample=None):
        """
        Create a new waveform from a data sequence and store it as
        *name*. Elements of *data* must be floating point values on the
        range [-1, 1].

        :param name: name for this waveform
        :type name: string
        :param vmax: maximum waveform voltage.
        :param data: data sequence
        :param fsample: sampling frequency or ``None`` to use the default.
        """
        if self._fsample <= 0:
            raise ValueError('Sampling frequency not set')
        if self._vpeak <= 0:
            raise ValueError('Voltage range not set')
        vmax = min(self._vpeak, vmax)
        # [-1, 1] -> [0, 4095]
        scale = 2048 * vmax / self._vpeak
        # Set the EOD bit on every data point because every value is
        # for the first channel.
        wf = array('H',
                   [max(min(int(x * scale) + 2048, 4095), 0) | DD_EOD
                    for x in data])
        if len(wf) > self.maxlen:
            raise ValueError('Waveform must contain <= {:d} points'.format(
                self.maxlen))
        # Set the EOM and EOF bits on last point
        wf[-1] = (wf[-1] & 0xfff) | DD_EOM | DD_EOF
        # Pad with zeros
        if len(wf) < self.minlen:
            n = self.minlen - len(wf)
            wf.extend([self.padval & 0xfff] * n)
            wf[-1] = wf[-1] | DD_EOM | DD_EOF
        self.waveforms[name] = Wave(fs=(fsample or self._fsample), data=wf)

    def arm(self, name, use_hardware=False, timeout=None):
        """
        Upload the named waveform to the board.

        :param name: waveform name
        :param use_hardware: if ``True`` use hardware trigger
        :param timeout: data transfer timeout in milliseconds.
        :return: number of bytes uploaded
        """
        if name not in self.waveforms:
            raise KeyError('Invalid waveform name')
        self.pause()
        self.dev.reset()
        if self.waveforms[name].fs:
            self.fsample = self.waveforms[name].fs
        n = self.dev.load_waveform(self.waveforms[name].data, timeout=timeout)
        if n != (len(self.waveforms[name]) * 2):
            raise RuntimeError('Waveform upload failed')
        if use_hardware:
            # Force output once PAUSE is released
            self.dev.enable()
        return n

    def trigger(self):
        """
        Start the waveform output.
        """
        self.dev.enable()
        self.unpause()
