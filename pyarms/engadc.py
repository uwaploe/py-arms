# -*- coding: utf-8 -*-
"""
.. module:: pyarms.engadc
     :platform: Linux
     :synopsis: interface to the engineering A/D converter.
"""
from decimal import Decimal


class Calibration(object):
    """
    Calibration coefficients for an A/D channel. Used to convert from
    input voltage to engineering units. The equation is of the form::

    .. math::

        y = C_0 + C_1x + C_2x^2 + ... + C_{N-1}x^{N-1}

    >>> x = 2
    >>> c = Calibration([0, 2], None)
    >>> c(x)
    (4, None)
    >>> c = Calibration([1, 2, 3], None)
    >>> c(x)
    (17, None)
    """
    def __init__(self, coeffs, units):
        """
        Instance initializer.

        :param coeffs: list of equation coefficients
        :param units: engineering units
        :type units: string
        """
        assert isinstance(coeffs, list)
        self.C = coeffs[:]
        self.C.reverse()
        self.units = units

    def __call__(self, x):
        """
        Apply the calibration equation to the input value.

        :return: a tuple of the converted value and the units
        """
        y = reduce(lambda a, b: a*x + b, self.C, 0)
        return y, self.units


def readadc(infile):
    """
    Read an A/D data record. The timestamp is in microseconds
    since the epoch.

    :param infile: A/D *procfs* file
    :return: tuple (timestamp, list of A/D values)
    """
    line = infile.read()
    vals = line.strip().split()
    ts = int(float(vals[0]) * 1000000)
    return ts, [Decimal(x) for x in vals[1:]]


def readeng(infile, cals, onlyvolts=False):
    """
    Sample all of the engineering data channels from the A/D. Each data
    record is a tuple of *NAME*, *VALUE*, *UNITS*.

    :param infile: A/D *procfs* file
    :param cals: list of calibration values
                 (output from :func:`config.load_calibration`)
    :param onlyvolts: if ``True``, do not appy calibration coefficients.
    :return: timestamp, list of data records
    """
    ts, counts = readadc(infile)
    # Use the first function to convert from counts to volts
    vcvt = cals[0][2]
    values = [vcvt(c)[0] for c in counts]
    rec = []
    if onlyvolts:
        for i, name, cvt in cals[1:]:
            rec.append((name, values[i], 'volts'))
    else:
        for i, name, cvt in cals[1:]:
            x, units = cvt(values[i])
            rec.append((name, x, units))
    return ts, rec
