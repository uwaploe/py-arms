# -*- coding: utf-8 -*-
"""Data acquisition software for ARMS"""

from pyarms import metadata


__version__ = metadata.version
__author__ = metadata.authors[0]
__license__ = metadata.license
__copyright__ = metadata.copyright
