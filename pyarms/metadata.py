# -*- coding: utf-8 -*-
"""Project metadata

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'pyarms'
project = "ARMS"
project_no_spaces = project.replace(' ', '')
version = '1.2.2'
description = 'Software for the Acoustic Reverberation Measurement System'
authors = ['Michael Kenney']
authors_string = ', '.join(authors)
emails = ['mikek@apl.uw.edu']
license = 'MIT'
copyright = '2022 ' + authors_string
url = ''
