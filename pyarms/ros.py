#!/usr/bin/env python
#
"""
.. module:: ros
   :synopsis: Interface to ROS single-axis rotators.
"""
import math


# Adjustment factor for travel time estimate
TT_ADJUST = 2.0
# Minimum travel time
TT_MIN = 3


def travel_time(x0, xp, v, a):
    """
    Estimate the travel time from *x0* to *xp* with an initial velocity of
    0, a maximum velocity of *v*, and an acceleration of *a*.
    """
    dx = abs(xp - x0)
    # Ramp-up time
    t1 = v / float(a)
    # Ramp-up distance
    x1 = t1 * v / 2.
    dx -= x1
    if dx <= 0:
        return TT_MIN + t1
    # Ramp-down distance
    dx -= x1
    if dx <= 0:
        return TT_MIN + t1 * 2.
    # Constant velocity time
    t2 = dx / float(v)
    return TT_MIN + (t2 + 2. * t1) * TT_ADJUST


class CommTimeout(Exception):
    """Raised when no response is received from the device."""
    pass


class Node(object):
    """
    Represent a node on an ROS serial network
    """
    def __init__(self, port, node_id='A'):
        """
        Initialize a new class instance.

        :param port: interface to serial port.
        :param node_id: rs485 network ID for this device
        """
        self.port = port
        self.node_id = node_id
        resp = self.send('?000', feedback=33)
        digits = '0123456789'
        self._settings = []
        for f in resp.split(','):
            try:
                if f[0] in digits:
                    self._settings.append(int(f, 10))
                else:
                    self._settings.append(f)
            except IndexError:
                raise ValueError('Empty response from device')

    def __repr__(self):
        return '{0}({1!r}, node_id={2!r})'.format(
            self.__class__.__name__,
            self.port, self.node_id)

    def send(self, command, feedback=0):
        """
        Send a command to the device and optionally read the feedback.

        :param command: command string
        :param feedback: number of characters expected
        :type feedback: integer or None
        :return: feedback from the device or None
        """
        text = self.node_id + command
        self.port.reset_input_buffer()
        for c in text:
            self.port.write(c)
            resp = self.port.read(1)
            if resp != c:
                raise CommTimeout
        if feedback:
            return self.port.read(feedback)
        else:
            return None


class Rotator(Node):
    """
    Class to provide an interface to an ROS rotator.
    """
    # step_size = 0.0102272727272727
    # Empirically determined
    step_size = 0.0056296358
    step_threshold = 36

    @classmethod
    def degrees_per_sec(cls, val):
        return val * 0.5

    @classmethod
    def counts_per_sec(cls, val):
        return int(val * 2)

    @property
    def factory_ccw(self):
        """
        The factory counter-clockwise limit in degrees
        """
        return self.degrees(self._settings[1])

    @property
    def factory_cw(self):
        """
        The factory clockwise limit in degrees
        """
        return self.degrees(self._settings[2])

    def degrees(self, counts):
        """
        Convert rotator counts to degrees of shaft rotation.
        """
        ccw = self._settings[1]
        cw = self._settings[2]
        return round(360 * float(counts - ccw) / float(cw - ccw), 1)

    def counts(self, degrees):
        """
        Convert degrees to rotator counts.
        """
        ccw = self._settings[1]
        cw = self._settings[2]
        scale = degrees / 360.
        return int(scale * (cw - ccw) + ccw + 0.5)

    def steps(self, degrees):
        """
        Convert degrees to motor steps.
        """
        return int(math.ceil(degrees/self.step_size))

    def start_cw(self, speed, ramp=True):
        """
        Start a clockwise rotation.

        :param speed: desired speed setting in deg/sec
        :param ramp: if ``True``, ramp speed up using the current
                     acceleration setting.
        :return: actual speed
        """
        cmd = ramp and '+' or '>'
        actual = Rotator.counts_per_sec(speed)
        self.send('{0}{1:03d}'.format(cmd, actual))
        return Rotator.degrees_per_sec(actual)

    def start_ccw(self, speed, ramp=True):
        """
        Start a counter-clockwise rotation.

        :param speed: desired speed setting in deg/sec
        :param ramp: if ``True``, ramp speed up using the current
                     acceleration setting.
        :return: actual speed
        """
        cmd = ramp and '-' or '<'
        actual = Rotator.counts_per_sec(speed)
        self.send('{0}{1:03d}'.format(cmd, actual))
        return Rotator.degrees_per_sec(actual)

    def stop(self, brake, ramp=True):
        """
        Stop the current rotation by applying a braking force.

        :param brake: brake force setting in counts
        :param ramp: if ``True``, ramp speed down using the current
                     acceleration setting.
        """
        cmd = ramp and 't' or 's'
        self.send('{0}{1:03d}'.format(cmd, brake))

    @property
    def counter(self):
        """
        Return the current counter value in steps.
        """
        resp = self.send('q', feedback=7)
        return int(resp[1:], 10)

    def reset_counter(self):
        """
        Reset the step counter.
        """
        self.send('z000')

    @property
    def angle(self):
        """
        The current rotator position in degrees.
        """
        resp = self.send('g', feedback=4)
        return self.degrees(int(resp[1:], 10))

    @property
    def ccw(self):
        """
        The user counter-clockwise limit in degrees
        """
        return self.degrees(self._settings[3])

    @ccw.setter
    def ccw(self, value):
        if not (self.factory_ccw <= value <= self.cw <= self.factory_cw):
            raise ValueError('Invalid user-ccw limit')
        value = self.counts(value)
        self.send('d{0:03d}'.format(value))
        self._settings[3] = value

    @property
    def cw(self):
        """
        The user clockwise limit in degrees
        """
        return self.degrees(self._settings[4])

    @cw.setter
    def cw(self, value):
        if not (self.factory_ccw <= self.ccw <= value <= self.factory_cw):
            raise ValueError('Bad user-cw limit')
        value = self.counts(value)
        self.send('u{0:03d}'.format(value))
        self._settings[4] = value

    def moveto(self, position):
        """
        Use the new positioning command to move the rotator to an
        absolute position. This operation uses the current ramp
        parameters (acceleration and maximum velocity).

        :param position: desired position in degrees.
        """
        if not (self.ccw <= position <= self.cw):
            raise ValueError('New position out of range')
        t = travel_time(self.angle, position, self.maxvel, self.accel)
        position = self.counts(position)
        self.send('p{0:03d}'.format(position))
        return t

    def stepto(self, position, speed):
        """
        Move to rotator to a new absolute position using the new
        precision rotation command.
        """
        if not (self.ccw <= position <= self.cw):
            raise ValueError('New position out of range')
        da = position - self.angle
        if da == 0:
            return 0
        if da < 0:
            direction = 0
        else:
            direction = 1
        da = abs(da)
        if da < self.step_threshold:
            return 0, ""
        t = travel_time(0, da, speed, self.accel)
        cmd = 'y{:d}{:02d}{:05d}'.format(
            direction,
            Rotator.counts_per_sec(speed),
            self.steps(da))
        self.send(cmd)
        return t, cmd

    def move(self, offset):
        """
        Adjust the position by *offset* degrees relative to the current
        position.

        :param offset: relative position in degrees.
        """
        return self.moveto(self.angle + offset)

    @property
    def accel(self):
        """
        The rotator acceleration value in deg/s^2
        """
        resp = self.send('?003', feedback=4)
        return (int(resp[1:], 10) + 1) * 2

    @accel.setter
    def accel(self, value):
        value = int(value / 2 - 1)
        if not (0 <= value <= 4):
            raise ValueError('Bad acceleration value')
        self.send('a{0:03d}'.format(value))

    @property
    def maxvel(self):
        """
        The maximum rotator velocity in deg/s
        """
        resp = self.send('?004', feedback=4)
        return self.degrees_per_sec(int(resp[1:], 10))

    @maxvel.setter
    def maxvel(self, value):
        value = self.counts_per_sec(value)
        if not (0 < value <= 80):
            raise ValueError('Bad velocity value')
        self.send('m{0:03d}'.format(value))

    @property
    def is_moving(self):
        """
        ``True`` if rotator is moving.
        """
        resp = self.send('?007', feedback=4)
        return resp[1:] == '001'

    @property
    def has_stalled(self):
        """
        ``True`` if rotator has stalled or slipped.
        """
        resp = self.send('?005', feedback=4)
        return resp[1:] == '001'

    @property
    def brake(self):
        """
        The rotator brake setting.
        """
        resp = self.send('?006', feedback=4)
        return int(resp[1:], 10)

    @brake.setter
    def brake(self, value):
        """
        Set the brake.
        """
        self.stop(value, ramp=False)

    @property
    def flags(self):
        """
        The rotator motion and stall status as a bitmask. Bit 0 is
        the motion status and Bit 1 is the stall status.
        """
        m_stat = self.send('?007', feedback=4)
        s_stat = self.send('?005', feedback=4)
        return int(s_stat[1:], 10) << 1 | int(m_stat[1:], 10)
