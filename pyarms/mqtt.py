# -*- coding: utf-8 -*-
#
import logging
from threading import Event
import paho.mqtt.client as mqtt


class Messenger(object):
    """
    Use an MQTT Broker to send and receive messages.
    """
    def __init__(self, msg_queue, host='localhost', port=1883,
                 sub=None, qos=1):
        """
        Initialize the object instance and open a threaded connection to
        the MQTT server.

        :param msg_queue: queue for incoming messages
        :type msg_queue: :class:`queue.Queue`
        :param host: server hostname or IP address
        :param port: server TCP port
        :param sub: subscription pattern
        :param qos: desired QOS for the subscription
        """
        self.client = mqtt.Client(protocol=mqtt.MQTTv31)
        self.mq = msg_queue
        self.sub = sub
        self.qos = qos
        self.client.on_connect = self._on_connect
        self.client.on_publish = self._on_publish
        self.client.on_message = self._on_message
        self.client.on_disconnect = self._on_disconnect
        self.logger = logging.getLogger()
        self.connected = Event()
        self.published = Event()
        self.client.connect(host, port)
        self.client.loop_start()

    def __del__(self):
        self.client.loop_stop()

    def _on_publish(self, client, udata, mid):
        self.logger.debug('Message %r published', mid)
        self.published.set()

    def _on_connect(self, client, udata, flags, rc):
        if rc == 0:
            if self.sub:
                self.client.subscribe(self.sub, qos=self.qos)
                self.logger.info('Subscribed to %r', self.sub)
            self.connected.set()

    def _on_message(self, client, udata, msg):
        self.logger.debug('> %r', msg)
        self.mq.put((msg.topic, msg.payload))

    def _on_disconnect(self, client, udata, rc):
        self.connected.clear()

    def publish(self, topic, msg, retain=False, timeout=5, qos=1):
        """
        Publish a message to the server and wait for confirmation.

        :param topic: message topic
        :param msg: message contents
        :param retain: if ``True`` server will retain this message
        :param timeout: number of seconds to wait for confirmation
        :return: message publish status, ``True`` or ``False``
        """
        status = self.connected.wait(5)
        if not status:
            self.logger.critical('Timeout connecting to server')
            raise IOError
        self.published.clear()
        self.logger.debug('Publishing to %s', topic)
        self.client.publish(topic,
                            payload=bytearray(msg),
                            qos=qos, retain=retain)
        return self.published.wait(timeout)
