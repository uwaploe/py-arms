# -*- coding: utf-8 -*-
import sys
from pyarms._adc import lib, ffi


CHAN_SEL = [(1 << i) for i in range(16)]
TRIG_MAP = {
    lib.TRIG_START: 'start',
    lib.TRIG_END: 'end'
}
NSEC_PER_SECOND = int(1e9)


def time_diffs(ts):
    """
    Utility function to calculate the forward-difference of the list
    of data-block time-stamps (see :meth:`BaseADC.read`). The
    units are microseconds.
    """
    t = [s*NSEC_PER_SECOND + ns for s, ns in ts]
    return [(x - y)//1000 for x, y in zip(t[1:], t[:-1])]


@ffi.def_extern()
def process_trigger(state, uuid, udata):
    """
    Pass trigger event information to :meth:`BaseADC.trigger_cb`.
    """
    adc = ffi.from_handle(udata)
    uuid = '' if uuid == ffi.NULL else ffi.string(uuid)
    adc.trigger_cb(TRIG_MAP.get(state, 'unknown'), uuid)


@ffi.def_extern()
def process_data(dp, mask, udata):
    """
    Pass sampled A/D values to :meth:`BaseADC.scan_cb`.
    """
    adc = ffi.from_handle(udata)
    # Store time-stamps for profiling
    adc.ts.append((dp.ts.tv_sec, dp.ts.tv_nsec))
    data = lib.adc_get_data(dp)
    for j in range(dp.scans):
        row = j * dp.chans
        scan = [data[i + row] for i in range(dp.chans)
                if (mask & CHAN_SEL[i])]
        adc.scan_cb(scan)


@ffi.def_extern()
def process_stream(dp, mask, udata):
    """
    Pass sampled A/D values to :meth:`BaseADC.stream_cb`.
    """
    adc = ffi.from_handle(udata)
    data = lib.adc_get_data(dp)
    stop = False
    for j in range(dp.scans):
        row = j * dp.chans
        scan = [data[i + row] for i in range(dp.chans)
                if (mask & CHAN_SEL[i])]
        stop = adc.stream_cb(scan)
        if stop:
            break
    return ffi.cast('int', stop)


class BaseADC(object):
    """
    Base class for controlling the General Standards A/D board. Provides
    a wrapper around libgsadc.

    Derived classes need to implement the :meth:`trigger_cb` and
    :meth:`scan_cb` in order to process the sampled data.
    """
    def __init__(self, chans, devname='/dev/24dsi16wrc.0', scanbuf=10):
        """
        Instance initializer.

        :param chans: active channel count or channel list
        :param devname: device name
        :param scanbuf: number of scans per block
        """
        if isinstance(chans, list):
            mask = reduce(lambda a, b: a | (1 << b), chans, 0)
        else:
            mask = (1 << chans) - 1
        dev = lib.adc_open(devname, mask, scanbuf)
        self.dev = ffi.gc(dev, lib.adc_close)
        self.n_scans = scanbuf
        udata = ffi.new_handle(self)
        self._udata = udata
        lib.adc_set_udata(self.dev, udata)
        self.v_peak = 0
        self.ts = []

    @property
    def mask(self):
        """
        Bitmask of active channels.
        """
        return lib.adc_get_mask(self.dev)

    @property
    def channels(self):
        """
        Number of channels sampled (>= number of active channels)
        """
        return lib.adc_get_nchannels(self.dev)

    def trigger_cb(self, state, uuid):
        """
        Callback for trigger events.

        :param state: ``start`` or ``end``
        :type state: string
        :param uuid: unique ID for trigger (only on ``start``)
        :type uuid: string
        """
        pass

    def scan_cb(self, scan):
        """
        Callback for A/D data

        :param scan: list of A/D values (integers)
        """
        pass

    def stream_cb(self, scan):
        """
        Callback for streaming A/D data.

        :param scan: list of A/D values (integers)
        :return: ``True`` if data collection should stop
        """
        return True

    def setup(self, fsample, v_peak):
        """
        Set the input parameters.

        :param fsample: desired sampling frequency in Hz
        :type fsample: int
        :param v_peak: peak input voltage in volts
        :return: actual sampling frequency in Hz
        """
        self.v_peak = v_peak
        fsample = lib.adc_init(self.dev, fsample, 10, v_peak)
        if fsample == 0:
            raise ValueError('Bad fsample value: {!r}'.format(fsample))
        return fsample

    def arm(self, timeout, use_hardware=False):
        """
        Prepare the board to be triggered.

        :param timeout: maximum wait for trigger in seconds
        :param use_hardware: if True, use external trigger.
        """
        r = lib.adc_arm(self.dev,
                        int(timeout * 1000),
                        ffi.cast('int', use_hardware))
        if r < 0:
            raise RuntimeError('Cannot arm A/D board')

    def trigger(self):
        """
        Start the data sampling process.
        """
        r = lib.adc_trigger(self.dev)
        if r < 0:
            raise RuntimeError('Trigger failed')

    def read(self, window):
        """
        Read analog input data. When this function returns, :attr:`ts` will
        contain a list of data-block timestamps.  Each timestamp is a
        tuple of (``seconds``, ``nanoseconds``).

        :param window: receive window size in samples.
        """
        self.ts = []
        lib.adc_read(self.dev,
                     window,
                     lib.process_data, lib.process_trigger)

    def stream(self):
        """
        Read analog input data continuously. Data collection stops when
        :meth:`stream_cb` returns ``True``.
        """
        lib.adc_stream(self.dev,
                       lib.process_stream, lib.process_trigger)


class ADC(BaseADC):
    """
    Example derived class to simply write the received
    data to standard output.
    """
    def trigger_cb(self, state, uuid):
        if state == 'start':
            sys.stdout.write('Window start {}\n'.format(uuid))
        else:
            sys.stdout.write('Window end\n')

    def scan_cb(self, scan):
        sys.stdout.write(' '.join([str(x) for x in scan]) + '\n')
