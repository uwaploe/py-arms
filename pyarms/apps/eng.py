#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import os
import pyarms.engadc
from pyarms.config import load_config, load_engconfig


def eng_data(fname, cals):
    """
    Read the engineering data and return in a format suitable
    for the HDF5 data file.
    """
    record = []
    with open(fname, 'rb') as infile:
        ts, results = pyarms.engadc.readeng(infile, cals)
        record = [ts] + [float(r[1]) for r in results]
    return tuple(record)


def readeng(cfgfile):
    """
    Sample the engineering A/D.

    :param cfgfile: A/D configuration file
    :type cfgfile: file object
    """
    path = os.environ.get('CFGPATH',
                          os.path.expanduser('~/config')).split(':')
    cfg = load_config(cfgfile, path=path)
    fname, cals = load_engconfig(cfg)
    with open(fname, 'rb') as infile:
        ts, results = pyarms.engadc.readeng(infile, cals)
    vals = []
    for name, val, units in results:
        vals.append('{}={!s}'.format(name, val))
    return 'eng {} {:d}'.format(','.join(vals), ts)


@click.command()
@click.argument('cfgfile', type=click.File('rb'))
def cli(cfgfile):
    """
    Read a single engineering A/D sample. CFGFILE contains the A/D
    calibration values.
    """
    click.echo(readeng(cfgfile))
