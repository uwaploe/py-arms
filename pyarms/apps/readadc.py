#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import time
import h5py
import array
import os
from threading import Timer
from pyarms.adc import BaseADC
from pyarms.config import Rcvr
from pyarms.datafile import File


def add_scalars(group, datasets):
    """
    Add one or more scalar data values to an HDF5 Group.
    """
    for name, val, dtype in datasets:
        group.create_dataset(name, (), data=val, dtype=dtype)


def find_dev():
    """
    Return the device path for the A/D board
    """
    if os.path.exists('/dev/24dsi.0'):
        return '/dev/24dsi.0'
    else:
        return '/dev/24dsi16wrc.0'


class AppADC(BaseADC):
    def __init__(self, channels, scanbuf):
        super(AppADC, self).__init__(channels, devname=find_dev(),
                                     scanbuf=scanbuf)
        self.fsample = 0

    def setup(self, fsample, vmax):
        # BaseADC.setup invokes the auto-calibration procedure so
        # don't run it unless we have to...
        if (fsample, vmax) != (self.fsample, self.v_peak):
            self.fsample = super(AppADC, self).setup(fsample, vmax)
        return self.fsample, self.v_peak

    def read(self, nsamples, filename):
        self.t0 = 0
        self.df = open(filename, 'wb')
        super(AppADC, self).read(nsamples)
        return self.t0

    def trigger_cb(self, state, uuid):
        if state == 'start':
            self.t0 = time.time()
            click.secho('Triggering', fg='green')
        elif state == 'end':
            self.df.close()

    def scan_cb(self, scan):
        array.array('i', scan).tofile(self.df)


def do_trigger(adc):
    try:
        adc.trigger()
    except RuntimeError:
        click.secho('Trigger failed, retrying...', fg='red')
        adc.trigger()


@click.command()
@click.option('--hwtrigger/--no-hwtrigger', default=False,
              help='use hardware trigger')
@click.option('--chans', default=12, type=int, help='input channel count')
@click.option('--vpeak', default=10, type=float, help='peak input voltage')
@click.option('--bufsize', default=100, type=int, help='scans to buffer')
@click.option('--pings', default=1, type=int, help='number of pings')
@click.argument('fsample', type=int)
@click.argument('duration')
@click.argument('output')
def cli(hwtrigger, chans, vpeak, bufsize, pings, fsample, duration, output):
    """Sample A/D input at FSAMPLE hz for the specified DURATION. The DURATION
    value is compatible with the Golang duration string format; a string of
    decimal numbers, each with an optional fraction and a unit suffix, such
    as '500ms', '1.2s', or '1m10s'.

    OUTPUT is an HDF5-format file which will contain all of the
    meta-data. The ping data will be written to a series of binary files
    named 'ping_NNNNNN.bin' in the same directory as OUTPUT. The ping data
    can be incorporated into the HDF5 file using the 'h5merge' program.
    """
    cfg = Rcvr(fsample=fsample, vmax=vpeak, window=duration, channels=range(chans))
    adc = AppADC(cfg.channels, bufsize)
    dirname = os.path.dirname(os.path.abspath(output))
    with h5py.File(output, 'w') as f:
        df = File(f)
        cfg.fsample, cfg.vmax = adc.setup(cfg.fsample, cfg.vmax)
        df.add_receiver(cfg)
        click.secho('Collecting {:d} samples @ {:d}hz'.format(
            cfg.nsamples, cfg.fsample), fg='green')
        while pings > 0:
            name = df.next_ping()
            adc.arm(30, use_hardware=hwtrigger)
            if not hwtrigger:
                t = Timer(1.0, do_trigger, args=(adc,))
                t.start()
            ts = adc.read(cfg.nsamples,
                          os.path.join(dirname, name) + '.bin')
            if ts > 0:
                df.add_ping(ts)
                click.secho(name, fg='green')
                pings -= 1
            else:
                click.secho('Trigger timeout!', fg='red')
                pings = 0
    click.echo(click.style('Done', fg='green'))
