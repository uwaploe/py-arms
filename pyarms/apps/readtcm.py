#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import os
import sys
import signal
import time
from multiprocessing import Process, Queue
import numpy as np
import xattr
import serial
from pyarms.tcm import Tcm6


def NS_USER(name):
    return 'user.' + name


if hasattr(xattr, 'set'):
    setxattr = xattr.set
else:
    setxattr = xattr.setxattr


def data_output(dq):
    """
    Process to write TCM data to standard output in
    Line Protocol format.
    """
    names = ('pitch', 'roll', 'heading')
    try:
        while True:
            item = dq.get()
            if isinstance(item, StopIteration):
                break
            ts = item[0]
            vals = []
            for k, v in zip(names, item[1:]):
                vals.append('{}={:.1f}'.format(k, v))
            sys.stdout.write('att {} {:d}\n'.format(','.join(vals), ts))
    except (KeyboardInterrupt, IOError):
        pass


def tcm_data(dev, dq=None):
    """
    Read a data record from the sensor and return in a format suitable
    for the HDF5 data file.
    """
    record = []
    ts = int(time.time() * 1000000)
    result = dev.kGetData()
    if result and result.id == 'kDataResp':
        record = [ts,
                  result.payload.kPAngle,
                  result.payload.kRAngle,
                  result.payload.kHeading]
        try:
            dq.put(record)
        except IOError:
            pass
    return tuple(record)


def sig_handler(signum, *args):
    if signum != signal.SIGALRM:
        sys.exit(1)


def readtcm(serialdev, interval, outfile, baud=38400, debug=False):
    """
    Sample the TCM attitude sensor.

    :param serialdev: serial port device
    :param interval: sampling interval in seconds.
    :param outfile: output file name or file object
    :param baud: serial baud rate
    :param debug: if ``True`` write data to stdout
    """
    port = serial.Serial(serialdev, baud)
    dev = Tcm6(port, timeout=3)
    try:
        # Sometimes the first command fails if the device
        # is in a "weird state".
        _ = dev.kGetModInfo()
    except IOError:
        pass
    dev.kSetAcqParams(polling=1,
                      flush=0,
                      acq_interval=0,
                      resp_interval=0)
    dev.kSetDataComponents(
        id=['kHeading', 'kPAngle', 'kRAngle'], count=3)
    proc, dq = None, None
    if debug:
        dq = Queue()
        proc = Process(target=data_output, args=(dq,))
        proc.daemon = True
        proc.start()

    # Create data-type description for Numpy record array
    dtype = [('time', 'i8')]
    for name in ('pitch', 'roll', 'heading'):
        dtype.append((name, 'f4'))
    data = []

    for sig in (signal.SIGTERM, signal.SIGALRM):
        signal.signal(sig, sig_handler)

    status = 1
    signal.setitimer(signal.ITIMER_REAL, interval, interval)
    try:
        while True:
            signal.pause()
            rec = tcm_data(dev, dq)
            if rec:
                data.append(rec)
    except (KeyboardInterrupt, SystemExit):
        click.secho('Exiting ...', fg='red')
        status = 0
    finally:
        signal.setitimer(signal.ITIMER_REAL, 0)
        if proc:
            try:
                dq.put(StopIteration())
                proc.join(3)
            except IOError:
                pass
        else:
            if os.path.exists(outfile):
                os.unlink(outfile)
            n = len(data)
            click.secho('Saving {:d} records ...'.format(n), fg='green')
            np.rec.array(data, dtype=dtype).tofile(outfile)
            setxattr(outfile, NS_USER('dims'), str(n))
            setxattr(outfile, NS_USER('dtype'), repr(dtype))
        return status


@click.command()
@click.option('-b', '--baud', default=38400, type=int,
              help='set serial baud rate')
@click.option('--debug/--no-debug', default=False)
@click.argument('serialdev')
@click.argument('interval', type=int)
@click.argument('outfile')
def cli(baud, debug, serialdev, interval, outfile):
    """
    Sample the TCM attitude sensor every INTERVAL seconds and write the
    data to OUTFILE in a format suitable for merging into an HDF5 data
    file.
    """
    readtcm(serialdev, interval, outfile, baud=baud, debug=debug)
