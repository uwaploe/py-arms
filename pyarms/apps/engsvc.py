#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import os
import sys
import signal
from multiprocessing import Process
from Queue import Queue, Empty
from pyarms.mqtt import Messenger
from pyarms.apps.readeng import readeng


def sig_handler(signum, *args):
    sys.exit(1)


def q_get(q):
    """
    Work-around a shortcoming in the Python 2 Queue implementation
    that disables signals in a blocking call to Queue.get().
    """
    while True:
        try:
            return q.get(timeout=300)
        except Empty:
            pass


@click.command()
@click.option('-o', '--open-topic',
              default='adc/events/open',
              help='MQTT topic for file open messages')
@click.option('-c', '--close-topic',
              default='adc/events/close',
              help='MQTT topic for file close messages')
@click.argument('cfgfile', type=click.File('rb'))
@click.argument('interval', type=int)
def cli(open_topic, close_topic, cfgfile, interval):
    """
    Listen for file-open/close messages from the acoustic data collection
    process and sample the engineering A/D while the acoustic data is being
    collected. The A/D is sampled every INTERVAL seconds and the data is
    written to a file which will be merged with the acoustic-data HDF5 file.
    CFGFILE contains the A/D calibration values.
    """
    # Find the common prefix for the two MQTT topics
    parts = []
    for s1, s2 in zip(open_topic.split('/'), close_topic.split('/')):
        if s1 == s2:
            parts.append(s1)
    parts.append('#')
    topic = '/'.join(parts)
    click.secho('Subscribing to {}'.format(topic), fg='green')
    mq = Queue()
    Messenger(mq, sub=topic, qos=2)
    signal.signal(signal.SIGTERM, sig_handler)
    proc = None
    try:
        while True:
            topic, payload = q_get(mq)
            if topic == open_topic:
                if proc:
                    proc.terminate()
                    proc.join(3)
                    click.secho(str(proc), fg='green')
                # The message payload contains the top-level directory for
                # the active data acquisition "sequence".
                fields = payload.split()
                fdir = os.path.join(fields[0], 'metadata', 'eng')
                if not os.path.exists(fdir):
                    os.makedirs(fdir)
                # Start a background process to sample the engineering
                # data.
                proc = Process(target=readeng,
                               args=(cfgfile, interval,
                                     os.path.join(fdir, 'timeseries')))
                proc.daemon = True
                proc.start()
                click.secho(str(proc), fg='green')
            elif topic == close_topic:
                if proc:
                    proc.terminate()
                    proc.join(3)
                    click.secho(str(proc), fg='green')
                proc = None
    except (KeyboardInterrupt, SystemExit):
        click.secho('Exiting ...', fg='red')
    finally:
        if proc and proc.is_alive():
            click.secho('Child process still active ...', fg='yellow')
            proc.terminate()
            proc.join(3)
            click.secho(str(proc), fg='green')
