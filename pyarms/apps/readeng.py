#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import os
import sys
import signal
from multiprocessing import Process, Queue
import numpy as np
import xattr
import pyarms.engadc
from pyarms.config import load_config, load_engconfig


def NS_USER(name):
    return 'user.' + name


if hasattr(xattr, 'set'):
    setxattr = xattr.set
else:
    setxattr = xattr.setxattr


def data_output(dq):
    """
    Process to write engineering data to standard output in
    Line Protocol format.
    """
    try:
        while True:
            item = dq.get()
            if isinstance(item, StopIteration):
                break
            ts, results = item
            vals = []
            for name, val, units in results:
                vals.append('{}={!s}'.format(name, val))
            sys.stdout.write('eng {} {:d}\n'.format(','.join(vals), ts))
    except (KeyboardInterrupt, IOError):
        pass


def eng_data(fname, cals, dq=None, onlyvolts=False):
    """
    Read the engineering data and return in a format suitable
    for the HDF5 data file.
    """
    record = []
    with open(fname, 'rb') as infile:
        ts, results = pyarms.engadc.readeng(infile, cals, onlyvolts=onlyvolts)
        if dq:
            dq.put((ts, results))
        record = [ts] + [float(r[1]) for r in results]
    return tuple(record)


def sig_handler(signum, *args):
    if signum != signal.SIGALRM:
        sys.exit(1)


def readeng(cfgfile, interval, outfile, debug=False, volts=False):
    """
    Sample the engineering A/D.

    :param cfgfile: A/D configuration file
    :type cfgfile: file object
    :param interval: sampling interval in seconds.
    :param outfile: output file name or file object
    """
    path = os.environ.get('CFGPATH',
                          os.path.expanduser('~/config')).split(':')
    cfg = load_config(cfgfile, path=path)
    fname, cals = load_engconfig(cfg)

    proc, dq = None, None
    if debug:
        dq = Queue()
        proc = Process(target=data_output, args=(dq,))
        proc.daemon = True
        proc.start()

    # Create data-type description for Numpy record array
    dtype = [('time', 'i8')]
    for chan, name, func in cals[1:]:
        dtype.append((name, 'f4'))
    data = []

    for sig in (signal.SIGTERM, signal.SIGALRM):
        signal.signal(sig, sig_handler)

    status = 1
    signal.setitimer(signal.ITIMER_REAL, interval, interval)
    try:
        while True:
            signal.pause()
            data.append(eng_data(fname, cals, dq, onlyvolts=volts))
    except (KeyboardInterrupt, SystemExit):
        click.secho('Exiting ...', fg='red')
        status = 0
    finally:
        signal.setitimer(signal.ITIMER_REAL, 0)
        if proc:
            try:
                dq.put(StopIteration())
                proc.join(3)
            except IOError:
                pass
        else:
            if os.path.exists(outfile):
                os.unlink(outfile)
            n = len(data)
            click.secho('Saving {:d} records ...'.format(n), fg='green')
            np.rec.array(data, dtype=dtype).tofile(outfile)
            setxattr(outfile, NS_USER('dims'), str(n))
            setxattr(outfile, NS_USER('dtype'), repr(dtype))
        return status


@click.command()
@click.option('--debug/--no-debug', default=False)
@click.option('--volts/--no-volts', default=False,
              help='output data values in A/D volts')
@click.argument('cfgfile', type=click.File('rb'))
@click.argument('interval', type=int)
@click.argument('outfile')
def cli(volts, debug, cfgfile, interval, outfile):
    """
    Sample the engineering A/D every INTERVAL seconds and write the data
    to OUTFILE in a format suitable for merging into an HDF5 data file.
    CFGFILE contains the A/D calibration values.
    """
    readeng(cfgfile, interval, outfile, debug, volts=volts)
