#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import time
import h5py
import array
import os
from Queue import Queue
from functools import partial
from contextlib import contextmanager
from multiprocessing import Process, Pipe
from threading import Timer, Event, Thread
import gcl
from mock import Mock
from serial import Serial
from pyarms.adc import BaseADC
from pyarms.mqtt import Messenger
from pyarms.config import Rcvr, load_config, load_engconfig,\
    load_attitude
from pyarms.apps.readadc import find_dev
from pyarms.tcm import Tcm6
from pyarms.apps.h5merge import merge_pings, merge_metadata
from pyarms.datafile import File
from pyarms.engadc import readeng


@contextmanager
def pushd(dir):
    cwd = os.getcwd()
    os.chdir(dir)
    try:
        yield
    finally:
        os.chdir(cwd)


def gen_basename():
    return time.strftime('%Y%m%d_%H%M%S', time.gmtime())


def data_packer(conn, compress=True, clean=True, move_to_parent=True):
    """
    Pack the acoustic data files into the HDF5 file.
    """
    while True:
        h5file = conn.recv()
        if h5file == '*done*':
            break
        dirname = os.path.dirname(os.path.abspath(h5file))
        click.secho('Packing {}'.format(h5file), fg='green')
        with h5py.File(h5file, 'r+') as f:
            comp_type = 'gzip' if compress else None
            with pushd(dirname):
                merge_pings(f, '/records', comp_type, clean)
                if os.path.isdir('metadata'):
                    merge_metadata(f, clean, binary=False)

        if move_to_parent:
            parent = os.path.dirname(dirname)
            os.rename(h5file, os.path.join(parent,
                                           os.path.basename(h5file)))
            if clean:
                click.secho('Removing {}'.format(dirname), fg='green')
                try:
                    os.rmdir(dirname)
                except OSError as e:
                    click.secho(str(e), fg='red')


class StreamFile(File):
    data_group = 'records'
    data_prefix = 'rec'
    add_record = File.add_ping
    next_record = File.next_ping


class AppADC(BaseADC):
    def __init__(self, channels, nrecs, datadir, scanbuf,
                 pub=None, feng=None, fatt=None):
        # Start data packer before initializing the A/D interface to save
        # memory in the child process.
        self.conn, child_conn = Pipe()
        self.proc = Process(target=data_packer, args=(child_conn,))
        self.proc.daemon = True
        self.proc.start()
        super(AppADC, self).__init__(channels, devname=find_dev(),
                                     scanbuf=scanbuf)
        self.f = None
        self.fdir = None
        self.df = None
        self.datadir = os.path.abspath(datadir)
        self.scan_count = 0
        self.nrecs = nrecs
        self.pub = pub
        self.ev = Event()
        self.feng = feng
        self.fatt = fatt

    def halt(self):
        click.secho('Halting', fg='red')
        self.ev.set()

    def stop_packer(self, timeout=10):
        self.conn.send('*done*')
        self.proc.join(timeout)
        if self.pub:
            self.pub('adc/events/done', '')

    def pack_data(self, filename):
        self.conn.send(filename)

    def _newrecord(self):
        """
        Start a new acoustic data record.
        """
        if self.df:
            self.df.close()
            self.df = None
        if len(self.f) == self.nrecs:
            # Time to start a new file
            self._newfile()
        else:
            self.scan_count = 0
            name = self.f.next_record()
            self.df = open(os.path.join(self.fdir, name) + '.bin', 'wb')
            self.f.add_record(time.time())

    def _newfile(self):
        """
        Start a new HDF5 output file.
        """
        if self.f:
            fname = self.f.filename
            if self.pub:
                self.pub('adc/events/close', self.fdir.encode('utf-8'))
            self.f.close()
            self.pack_data(fname)
            self.f = None
        base = gen_basename()
        # Directory for acoustic data
        self.fdir = os.path.join(self.datadir, base)
        if not os.path.exists(self.fdir):
            os.makedirs(self.fdir)
        self.f = StreamFile(h5py.File(
            os.path.join(self.fdir, base + '.h5'), 'w'))
        self.f.add_receiver(self.cfg)
        # Read engineering A/D
        if self.feng:
            ts, eng = self.feng()
            self.f.add_eng(eng)
        # Read attitude sensor
        if self.fatt:
            ts, att = self.fatt()
            self.f.add_eng(att)
        if self.pub:
            self.pub('adc/events/open', self.fdir.encode('utf-8'))
        self._newrecord()

    def setup(self, fsample, v_peak):
        fsample = super(AppADC, self).setup(fsample, v_peak)
        return fsample, self.v_peak

    def arm(self, cfg, *args, **kwds):
        self.cfg = cfg
        self.nsamples = cfg.nsamples
        self._newfile()
        super(AppADC, self).arm(*args, **kwds)

    def trigger_cb(self, state, uuid):
        if state == 'end':
            if self.df:
                self.df.close()
                self.df = None
            if self.f:
                fname = self.f.filename
                self.f.close()
                self.pack_data(fname)
                self.f = None

    def stream_cb(self, scan):
        array.array('i', scan).tofile(self.df)
        self.scan_count += 1
        if self.scan_count == self.nsamples:
            self._newrecord()
        return self.ev.is_set()


def halt_stream(mq, halt_func):
    topic, msg = mq.get()
    if msg == 'halt':
        halt_func()


def setup_tcm(desc):
    click.secho('Configuring attitude sensor', fg='green')
    try:
        port = Serial(desc.device, desc.baudrate)
        dev = Tcm6(port, timeout=3)
        # If this is the first communication attempt with the
        # device after power-up, the first packet might be
        # misinterpreted so we allow for one failure.
        try:
            _ = dev.kGetModInfo()
        except IOError:
            _ = dev.kGetModInfo()
    except IOError:
        click.secho('Cannot access attitude sensor', fg='red')
        dev = Mock()
        dev.kGetData = Mock(return_value=None)
    else:
        dev.kSetAcqParams(polling=1,
                          flush=0,
                          acq_interval=0,
                          resp_interval=0)
        dev.kSetDataComponents(
            id=['kHeading', 'kPAngle', 'kRAngle'], count=3)
        if 'orientation' in desc.settings:
            dev.kSetConfig(id='kMountingRef',
                           value=desc.settings['orientation'])
    return dev


def engadc_data(fname, cals):
    """
    Read the engineering data and return in a format suitable
    for the HDF5 data file.
    """
    data = []
    ts = int(time.time())
    with open(fname, 'rb') as infile:
        ts, results = readeng(infile, cals)
        for name, val, units in results:
            data.append([name, float(val), 'f4', {'units': units}])
    return ts, data


def attitude_data(dev):
    """
    Read the attitude sensor data and return in a format suitable
    for the HDF5 data file.
    """
    ts = int(time.time())
    result = dev.kGetData()
    if result and result.id == 'kDataResp':
        data = [
            ['pitch', result.payload.kPAngle, 'f4', {'units': 'degrees'}],
            ['roll', result.payload.kRAngle, 'f4', {'units': 'degrees'}],
            ['heading', result.payload.kHeading, 'f4', {'units': 'degrees'}]
        ]
    else:
        data = [
            ['pitch', -99., 'f4', {'units': 'degrees'}],
            ['roll', -99., 'f4', {'units': 'degrees'}],
            ['heading', -99., 'f4', {'units': 'degrees'}]
        ]
    return ts, data


@click.command()
@click.option('-b', '--bufsize', default=15000,
              help='A/D buffer size in scans')
@click.argument('cfgfile', type=click.File('rb'))
@click.argument('output')
def cli(bufsize, cfgfile, output):
    """
    Stream A/D data to a series of HDF5 files in the directory
    OUTPUT. The sampling parameters are defined in CFGFILE.

    To stop the data collection, send a 'halt' message via MQTT as
    follows:

      mosquitto_pub -t adc/commands -m halt
    """
    if not os.path.isdir(output):
        raise SystemExit('Output directory does not exist')
    mq = Queue()
    m = Messenger(mq, sub='adc/commands')
    path = os.environ.get('CFGPATH',
                          os.path.expanduser('~/config')).split(':')
    cfg = load_config(cfgfile, path=path)

    fname, cals = load_engconfig(cfg)
    feng = partial(engadc_data, fname, cals)
    tcm = setup_tcm(load_attitude(cfg))
    fatt = partial(attitude_data, tcm)

    rcvr = Rcvr(**(gcl.util.to_python(cfg['sample']['rcvr'])))
    adc = AppADC(rcvr.channels,
                 cfg['sample']['records'],
                 output,
                 bufsize,
                 feng=feng,
                 fatt=fatt,
                 pub=partial(m.publish, timeout=None, qos=2))
    rcvr.fsample, rcvr.vmax = adc.setup(rcvr.fsample, rcvr.vmax)
    adc.arm(rcvr, 10)
    t = Thread(target=halt_stream, args=(mq, adc.halt))
    t.daemon = True
    t.start()
    t = Timer(1.0, adc.trigger)
    t.start()
    click.secho('Starting stream', fg='green')
    try:
        adc.stream()
    except Exception as e:
        click.secho(str(e), fg='red')
    finally:
        adc.stop_packer()
