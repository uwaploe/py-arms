#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import h5py
import numpy as np
from pyarms.config import parse_duration


@click.command()
@click.option('--volts/--no-volts', default=False)
@click.option('--sep', default=' ', help='column separator in output file')
@click.option('--tstart', default='0s', help='window start time')
@click.option('--group', default='/pings/ping_000001',
              help='name of HDF5 group containing ping data')
@click.argument('duration')
@click.argument('h5file', type=click.Path(exists=True))
@click.argument('output', type=click.File('wb'))
def cli(volts, sep, tstart, group, duration, h5file, output):
    """
    Extract a subset of the ping data from an ARMS HDF5 data file.

    DURATION specifies the size of the data-set. The data values
    are written to OUTPUT in ascii.
    """
    f = h5py.File(h5file, 'r')
    fs = f['/metadata/rcvr/fsample'][()]
    vscale = f['/metadata/rcvr/vscale'][()]
    i_start = int(parse_duration(tstart) * fs / 1000000)
    ns = int(parse_duration(duration) * fs / 1000000)
    ping = f[group + '/signal'][i_start:i_start+ns, :]
    if volts:
        np.savetxt(output, ping*vscale, fmt='%.6f', delimiter=sep)
    else:
        np.savetxt(output, ping, fmt='%d', delimiter=sep)
