#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import time
import subprocess
import signal
import os
import sys
import json
from Queue import Queue, Empty
from datetime import datetime
from dateutil.parser import parse
from dateutil.tz import tzutc
from serial import Serial
from pyarms.mqtt import Messenger
from pyarms.ros import Rotator, CommTimeout
from pyarms.config import load_config, load_rotator, \
    parse_schedule


EPOCH = datetime(1970, 1, 1, tzinfo=tzutc())


def setup_rotator(desc):
    click.secho('Configuring rotator', fg='green')
    try:
        port = Serial(desc.device, desc.baudrate, timeout=3)
        rot = Rotator(port)
    except (ValueError, IOError, CommTimeout):
        click.secho('Cannot access rotator', fg='red')
        return None
    else:
        for k, v in desc.settings.items():
            try:
                if getattr(rot, k) is not None:
                    setattr(rot, k, v)
            except AttributeError:
                pass
    return rot


def rotator_wait(rot, t_limit):
    while time.time() < t_limit:
        status = rot.flags
        # Check for stall
        if status & 2:
            break
        # Check for motor stop
        if (status & 1) == 0:
            break
        time.sleep(0.5)
    return rot.flags


def park_rotator(desc):
    """
    Move rotator to its parking position.
    """
    park = desc.settings.get('park')
    brake = desc.settings.get('brake', 80)
    if park:
        rot = setup_rotator(desc)
        if rot:
            click.secho('Rotating to PARK position', fg='yellow')
            try:
                dt, _ = rot.stepto(park, rot.maxvel)
                flags = rotator_wait(rot, time.time() + dt*3)
                if flags != 0:
                    click.secho('PARK position not reached ({:02d})'.format(
                        flags), fg='red')
            finally:
                rot.stop(brake)


def tosecs(interval):
    """
    Convert an HH:MM string to integer seconds.
    """
    hours, minutes = [int(t, 10) for t in interval.split(':')]
    return ((hours * 60) + minutes) * 60


def gen_seqname(t):
    return time.strftime('seq_%Y%m%d_%H%M%S', time.gmtime(t))


def sig_handler(signum, *args):
    if signum not in (signal.SIGALRM, signal.SIGCHLD):
        sys.exit(1)


def proc_monitor(proc, t_limit):
    """
    Wait for a child process to exit or for a time limit to expire.
    """
    for sig in (signal.SIGCHLD, signal.SIGALRM, signal.SIGTERM):
        signal.signal(sig, sig_handler)
    try:
        signal.alarm(t_limit)
        signal.pause()
    finally:
        signal.alarm(0)
        for sig in (signal.SIGCHLD, signal.SIGALRM, signal.SIGTERM):
            signal.signal(sig, signal.SIG_DFL)
        if proc.poll() is None:
            proc.terminate()
            time.sleep(3)
            proc.kill()
        return proc.wait()


def run_entry(entry, pub, minsleep=60):
    """
    Run a schedule entry
    """
    if entry['reverse'] in ('1', 'yes', 'on', 'true'):
        rev = '--rev'
    else:
        rev = '--no-rev'
    cmd = [
        'chrt', '-f', '90',
        'runseq', rev, '--bufsize', '15000',
        entry['name'],
        os.path.expanduser('~/data')
    ]
    interval = tosecs(entry['interval'])
    t_limit = tosecs(entry['timeout'])
    n = int(entry['count'])
    while n > 0:
        t = int(time.time())
        if interval > 0:
            tnext = (1 + (t // interval)) * interval
        else:
            tnext = t
        if (tnext - time.time()) > minsleep:
            # low-power sleep mode
            click.secho('Suspending until {:d}'.format(tnext))
            pub('system/lpmode/mem', str(int(tnext)).encode('utf-8'))
            # Wait for the suspend request to take effect
            while time.time() < tnext:
                time.sleep(1)
        else:
            try:
                time.sleep(tnext - time.time())
            except IOError:
                pass
        # Create directory for this sequence.
        dirname = os.path.join(os.path.expanduser('~/data'),
                               gen_seqname(tnext))
        if not os.path.isdir(dirname):
            os.makedirs(dirname)
        # Add to command invocation
        cmd[-1] = dirname
        try:
            proc = subprocess.Popen(cmd)
        except Exception as e:
            click.secho('Runseq failed ({})'.format(str(e)), fg='red')
            status = -1
        else:
            if t_limit == 0:
                status = proc.wait()
            else:
                status = proc_monitor(proc, t_limit)
            click.secho('Seq {name} done ({status:d})'.format(
                    status=status, **entry), fg='green')
        if status == 2:
            # Low battery, abort schedule
            raise SystemExit(1)
        elif status != 0:
            # Other error condition, abort sequence
            break
        n -= 1


class DateTimeParamType(click.ParamType):
    name = 'datetime'

    def convert(self, value, param, ctx):
        try:
            d = parse(value)
            if d.tzinfo:
                return d
            else:
                return d.replace(tzinfo=tzutc())
        except Exception:
            self.fail('%s not a valid date-time' % value,
                      param, ctx)

    def __repr__(self):
        return 'DATETIME'


DATETIME = DateTimeParamType()


@click.command()
@click.option('--start', type=DATETIME,
              help='specify the start date-time')
@click.argument('cfgfile', type=click.File('rb'))
@click.argument('infile')
def cli(start, cfgfile, infile):
    """
    Run an ARMS sequence schedule. The schedule is read from INFILE.
    """
    path = os.environ.get('CFGPATH',
                          os.path.expanduser('~/config')).split(':')
    cfg = load_config(cfgfile, path=path)

    # Add virtualenv bin directory to PATH env variable
    if os.path.isdir(os.path.expanduser('~/venv/bin')):
        path = os.environ.get('PATH').split(':')
        path.insert(0, os.path.expanduser('~/venv/bin'))
        os.environ['PATH'] = ':'.join(path)

    rows = []
    with open(infile, 'rb') as f:
        rows = list(parse_schedule(f))

    mq = Queue()
    m = Messenger(mq, sub='schedule/commands/+')
    now = datetime.now(tzutc())
    if start and (start > now):
        delta = start - EPOCH
        click.secho('Suspending until {}'.format(str(start)))
        secs = int(delta.total_seconds())
        m.publish('system/lpmode/mem', str(secs).encode('utf-8'))
        # Wait for the suspend request to take effect
        while time.time() < secs:
            time.sleep(1)
    try:
        while True:
            try:
                row = rows.pop(0)
            except IndexError:
                break
            try:
                topic, payload = mq.get_nowait()
            except Empty:
                pass
            else:
                cmd = topic.split('/')[-1]
                if cmd == 'halt':
                    raise SystemExit(1)
                elif cmd == 'update':
                    try:
                        newrow = json.loads(payload)
                    except Exception as e:
                        click.secho('Bad schedule format: {}'.format(str(e)),
                                    fg='red')
                    else:
                        rows.insert(0, newrow)
            run_entry(row, m.publish)
    finally:
        park_rotator(load_rotator(cfg))
