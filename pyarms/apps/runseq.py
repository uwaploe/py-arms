#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import time
import h5py
import os
import sys
import signal
import struct
import ctypes
import shutil
from multiprocessing import Process, Pipe
from Queue import Queue, Empty
from contextlib import contextmanager
from functools import partial
from threading import Timer
from serial import Serial
from mock import Mock
from pyarms.mqtt import Messenger
from pyarms.dac import ArbGen, USBDA128A
from pyarms.config import parse_sequence, load_config, \
    load_engconfig, load_rotator, load_attitude
from pyarms.datafile import File
from pyarms.ros import Rotator, CommTimeout
from pyarms.tcm import Tcm6
from pyarms.engadc import readeng
from pyarms.apps.readadc import AppADC
from pyarms.apps.h5merge import merge_pings, merge_metadata


SCHED_OTHER = ctypes.c_int(0)
SCHED_FIFO = ctypes.c_int(1)
SCHED_RR = ctypes.c_int(2)


def set_prio(policy, value):
    """
    Set scheduling policy and (static) priority.
    """
    libc = ctypes.CDLL('libc.so.6')
    param = ctypes.c_int(value)
    err = libc.sched_setscheduler(os.getpid(), policy, ctypes.byref(param))
    if err != 0:
        raise RuntimeError('errno = {:d}'.format(err))


class RotatorTimeout(Exception):
    pass


@contextmanager
def pushd(dir):
    cwd = os.getcwd()
    os.chdir(dir)
    try:
        yield
    finally:
        os.chdir(cwd)


def gen_basename():
    return time.strftime('%Y%m%d_%H%M%S', time.gmtime())


def data_packer(conn, compress=False, clean=True, move_to_parent=True):
    """
    Worker process to pack the acoustic data files into the HDF5 file.
    """
    set_prio(SCHED_OTHER, 0)
    os.nice(5)
    while True:
        h5file = conn.recv()
        if h5file == '*done*':
            break
        dirname = os.path.dirname(os.path.abspath(h5file))
        click.secho('Packing {}'.format(h5file), fg='green')
        with h5py.File(h5file, 'r+') as f:
            comp_type = 'gzip' if compress else None
            with pushd(dirname):
                merge_pings(f, '/pings', comp_type, clean)
                if os.path.isdir('metadata'):
                    merge_metadata(f, clean, binary=False)

        if move_to_parent:
            parent = os.path.dirname(dirname)
            os.rename(h5file, os.path.join(parent,
                                           os.path.basename(h5file)))
            if clean:
                click.secho('Removing {}'.format(dirname), fg='green')
                try:
                    os.rmdir(dirname)
                except OSError as e:
                    click.secho(str(e), fg='red')


def setup_tcm(desc):
    click.secho('Configuring attitude sensor', fg='green')
    try:
        port = Serial(desc.device, desc.baudrate)
        dev = Tcm6(port, timeout=1)
        # If this is the first communication attempt with the
        # device after power-up, the first packet might be
        # misinterpreted so we allow for one failure.
        try:
            _ = dev.kGetModInfo()
        except IOError:
            _ = dev.kGetModInfo()
    except IOError:
        click.secho('Cannot access attitude sensor', fg='red')
        dev = Mock()
        dev.kGetData = Mock(return_value=None)
    else:
        dev.timeout = 3
        dev.kSetAcqParams(polling=1,
                          flush=0,
                          acq_interval=0,
                          resp_interval=0)
        dev.kSetDataComponents(
            id=['kHeading', 'kPAngle', 'kRAngle'], count=3)
        if 'orientation' in desc.settings:
            dev.kSetConfig(id='kMountingRef',
                           value=desc.settings['orientation'])
    return dev


def setup_rotator(desc):
    click.secho('Configuring rotator', fg='green')
    try:
        port = Serial(desc.device, desc.baudrate, timeout=3)
        rot = Rotator(port)
    except (ValueError, IOError, CommTimeout):
        click.secho('Cannot access rotator', fg='red')
        rot = Mock()
        rot.moveto = Mock(return_value=2.5)
        rot.stepto = Mock(return_value=(2.5, ''))
        rot.reset_counter = Mock()
        rot.counter = 0
        rot.maxvel = 5
        rot.angle = -1
        rot.flags = 0
    else:
        for k, v in desc.settings.items():
            try:
                if getattr(rot, k) is not None:
                    setattr(rot, k, v)
            except AttributeError:
                pass
    return rot


def rotator_wait(rot, t_limit):
    while time.time() < t_limit:
        status = rot.flags
        if status & 2:
            return rot.flags
        if (status & 1) == 0:
            return rot.flags
        time.sleep(0.5)
    raise RotatorTimeout('Timeout')


def amp_gate(line, state):
    mask = 1 << line
    value = mask if state else 0
    with open('/proc/iguana/dio', 'wb') as f:
        f.write(struct.pack('<HH', value, mask))


def setup_xmtr(ag, cfg):
    click.secho('Configuring Arbgen board', fg='green')
    ag.fsample = cfg.fsample
    ag.vpeak = 2.5
    if cfg.enable and (cfg.waveform not in ag):
        if not cfg.data:
            raise RuntimeError('Waveform data not available')
        ag.add_waveform(cfg.waveform, cfg.vmax, cfg.data, cfg.fsample)


def setup_rcvr(adc, cfg):
    click.secho('Configuring ADC board', fg='green')
    cfg.fsample, cfg.vmax = adc.setup(cfg.fsample, cfg.vmax)


def do_trigger(adc=None, ag=None):
    click.echo('Sending trigger')
    if adc:
        try:
            adc.trigger()
        except RuntimeError:
            click.secho('ADC trigger failed. Retrying', fg='yellow')
            adc.trigger()
    if ag:
        ag.trigger()


@contextmanager
def new_sequence(msg, filename, pub, f_after):
    """
    Context manager for a sequence.
    """
    pub('seq/events/open', msg.encode('utf-8'))
    f = h5py.File(filename, 'w')
    try:
        yield f
    finally:
        pub('seq/events/close', msg.encode('utf-8'))
        f.close()
        time.sleep(1)
        if f_after:
            f_after()


def collect_data(df, adc, ag, sample, datadir,
                 fgate=None, fcancel=None):
    """
    Collect and store data for a sequence.

    :param df: datafile.File object
    :param adc: ADC object
    :param ag: Arbgen object
    :param sample: configuration info for the sample.
    :param datadir: data directory.
    :param fgate: GATE function
    :param fcancel: cancellation function.
    """
    while sample.pings > 0:
        if fcancel and fcancel():
            break
        name = df.next_ping()
        if sample.xmtr.enable:
            ag.arm(sample.xmtr.waveform, timeout=3000)
        if sample.rcvr.enable:
            adc.arm(30)
            time.sleep(0.1)
        # Enable the GATE and start a timer to disable it after
        # the waveform has been transmitted.
        if fgate and sample.xmtr.enable:
            fgate(True)
            dt = sample.xmtr.pulselen * 1e-6
            tmr = Timer(dt*1.1, fgate, args=(False,))
            tmr.start()
        do_trigger(sample.rcvr.enable and adc,
                   sample.xmtr.enable and ag)
        if sample.rcvr.enable:
            ts = adc.read(sample.rcvr.nsamples,
                          os.path.join(datadir, name + '.bin'))
            if ts > 0:
                df.add_ping(ts)
                click.secho(name, fg='green')
                sample.pings -= 1
            else:
                click.secho('Trigger timeout!', fg='red')
                sample.pings = 0
        else:
            sample.pings -= 1
        # Make sure the GATE is off
        if fgate and sample.xmtr.enable:
            tmr.cancel()
            fgate(False)
    return sample.pings


def eng_message(eng, t, tag='eng'):
    """
    Create an engineering data message to send via MQTT
    """
    vals = []
    for name, val, dtype, attrs in eng:
        vals.append('{}={:.3f}'.format(name, val))
    return '{} {} {:d}'.format(tag, ','.join(vals), t)


def sig_handler(signum, *args):
    sys.exit(1)


def check_halt(mq):
    try:
        topic, payload = mq.get_nowait()
    except Empty:
        pass
    else:
        cmd = topic.split('/')[-1]
        if cmd == 'halt':
            return True
    return False


def runseq(steps, outdir, bufsize, rotcfg,
           feng=None, fatt=None, minbatt=0, bias=0,
           compress=False):
    """
    Run an ARMS data collection sequence. Creates an HDF5 data file
    for each step of the sequence. This function starts a separate
    process (see :func:`data_packer`) to merge the pulse data into
    the HDF5 file.

    :param steps: list of :class:`pyarms.config.Step` objects
    :param outdir: data directory
    :param bufsize: A/D buffer size (in scans)
    :param rotcfg: Rotator configuration
    :param feng: function to read engineering data.
    :param bias: adjustment to each Rotator step angle
    :param compress: if ``True``, compress receiver data
    """
    rot = setup_rotator(rotcfg)
    adc = AppADC(steps[0].sample.rcvr.channels, bufsize)
    ag = ArbGen(USBDA128A())
    conn, child_conn = Pipe()
    proc = Process(target=data_packer,
                   args=(child_conn,),
                   kwargs={'compress': compress})
    proc.daemon = True
    proc.start()

    mq = Queue()
    m = Messenger(mq, sub='seq/commands/+', qos=1)
    fcancel = partial(check_halt, mq)
    status = 0
    rot.reset_counter()
    signal.signal(signal.SIGTERM, sig_handler)
    try:
        for step in steps:
            sample = step.sample
            if check_halt(mq):
                click.secho('Got "halt" command', fg='yellow')
                raise SystemExit(1)
            angle = step.angle + bias
            if angle >= 0:
                dt, _ = rot.stepto(angle, rot.maxvel)
                t_limit = time.time() + dt * 2.5
            setup_xmtr(ag, sample.xmtr)
            setup_rcvr(adc, sample.rcvr)
            base = gen_basename()
            datadir = os.path.join(outdir, base)
            if not os.path.exists(datadir):
                os.makedirs(datadir)
            try:
                if angle >= 0 and dt > 0:
                    flags = rotator_wait(rot, t_limit)
                    if flags & 0x02:
                        click.secho('Rotator stall detected', fg='red')
                        m.publish('seq/events/error', 'rotator stall')
            except RotatorTimeout:
                click.secho('Rotator timeout', fg='yellow')
                m.publish('seq/events/error', 'rotator timeout')
            except Exception as e:
                msg = repr(e)
                click.secho(msg, fg='red')
                m.publish('seq/events/error', msg.encode('utf-8'))
                os.rmdir(datadir)
                continue

            m.publish('seq/events/step', str(rot.angle).encode('utf-8'))

            # List to hold engineering data sets
            eng = []
            if feng:
                ts, eng = feng()
                if minbatt:
                    d = {e[0]: e[1] for e in eng}
                    if d.get('vbatt', 0) < minbatt:
                        click.secho('Minimum battery threshold reached',
                                    fg='red')
                        m.publish('seq/events/error', 'low voltage')
                        status = 2
                        break
                eng.append(['angle', float(rot.angle), 'f4',
                            {'units': 'degrees'}])
                eng.append(['counter', int(rot.counter), 'i4',
                            {'units': 'steps'}])
                m.publish('eng/data',
                          eng_message(eng, ts).encode('utf-8'))

            if fatt:
                ts, att = fatt()
                # Add the attitude sensor data
                eng.extend(att)
                m.publish('eng/data',
                          eng_message(att, ts, tag='att').encode('utf-8'))

            if sample.xmtr.gate >= 0:
                fgate = partial(amp_gate, sample.xmtr.gate)
            else:
                fgate = None
            filename = os.path.join(datadir, base + '.h5')
            msg = ' '.join([datadir, str(sample.duration)])
            with new_sequence(msg, filename, m.publish,
                              partial(conn.send, filename)) as f:
                df = File(f)
                df.add_receiver(sample.rcvr)
                df.add_transmitter(sample.xmtr)
                df.add_eng(eng)
                remaining = collect_data(df, adc, ag, sample, datadir,
                                         fgate=fgate, fcancel=fcancel)
                if remaining != 0:
                    click.secho('Got "halt" message', fg='yellow')
                    raise SystemExit(1)
    except (KeyboardInterrupt, SystemExit) as e:
        click.secho('Interrupted ...', fg='red')
        status = e.code if hasattr(e, 'code') else 1
    finally:
        conn.send('*done*')
        click.secho('Waiting for data packer to finish', fg='yellow')
        proc.join()
        m.publish('seq/events/done', '')
        return status


def engadc_data(fname, cals):
    """
    Read the engineering data and return in a format suitable
    for the HDF5 data file.
    """
    data = []
    ts = int(time.time())
    with open(fname, 'rb') as infile:
        ts, results = readeng(infile, cals)
        for name, val, units in results:
            data.append([name, float(val), 'f4', {'units': units}])
    return ts, data


def attitude_data(dev):
    """
    Read the attitude sensor data and return in a format suitable
    for the HDF5 data file.
    """
    ts = int(time.time())
    result = dev.kGetData()
    if result and result.id == 'kDataResp':
        data = [
            ['pitch', result.payload.kPAngle, 'f4', {'units': 'degrees'}],
            ['roll', result.payload.kRAngle, 'f4', {'units': 'degrees'}],
            ['heading', result.payload.kHeading, 'f4', {'units': 'degrees'}]
        ]
    else:
        data = [
            ['pitch', -99., 'f4', {'units': 'degrees'}],
            ['roll', -99., 'f4', {'units': 'degrees'}],
            ['heading', -99., 'f4', {'units': 'degrees'}]
        ]
    return ts, data


@click.command()
@click.option('--vmin', default=0, type=float,
              help='minimum battery voltage')
@click.option('--compress/--no-compress', default=False,
              help='compress the receiver data-sets')
@click.option('--bias', default=0, type=int,
              help='adjustment to add to each rotator step angle')
@click.option('--rev/--no-rev', default=False,
              help='also run the reverse sequence')
@click.option('-b', '--bufsize', default=15000,
              help='A/D buffer size in scans')
@click.argument('cfgfile', type=click.File('rb'))
@click.argument('outdir')
def cli(vmin, compress, bias, rev, bufsize, cfgfile, outdir):
    """Run an ARMS data collection sequence. A sequence consists of one or
    more acoustic pings transmitted and received at one or more Rotator
    steps. CFGFILE is the configuration file which describes the sequence,
    OUTDIR is the data directory which will contain the output files.
    """
    outdir = os.path.abspath(outdir)
    path = os.environ.get('CFGPATH',
                          os.path.expanduser('~/config')).split(':')
    cfg = load_config(cfgfile, path=path)
    steps, settings = parse_sequence(cfg)
    if rev:
        rsteps, _ = parse_sequence(cfg, rev=True)
        # Skip the first step in the reverse sequence since it is
        # identical to the last step in the forward sequence.
        steps.extend(rsteps[1:])
    click.secho('Sequence length: {:d} steps'.format(len(steps)),
                fg='green')
    fname, cals = load_engconfig(cfg)
    tcm = setup_tcm(load_attitude(cfg))
    feng = partial(engadc_data, fname, cals)
    # The first engineering A/D sample is usually bogus, read it
    # and discard
    _ = feng()
    fatt = partial(attitude_data, tcm)
    if vmin == 0:
        vmin = settings.get('vmin', 0)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    try:
        shutil.copy(os.path.join(os.path.expanduser('~/config'), cfgfile.name),
                    outdir)
    except (IOError, os.error) as e:
        click.secho('File copy failed: {!r}'.format(e), fg='red')
    # Copy configuration file to the output directory
    sys.exit(runseq(steps, outdir, bufsize, load_rotator(cfg),
                    minbatt=vmin,
                    feng=feng, fatt=fatt, bias=bias,
                    compress=compress))
