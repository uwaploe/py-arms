#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import time
import struct
from contextlib import contextmanager
import numpy as np
from pyarms.dac import ArbGen, USBDA128A
from pyarms.pulse import fmslide, tone


@contextmanager
def amp_gate(line, thold):
    if line >= 0:
        mask = 1 << line
        with open('/proc/iguana/dio', 'wb') as f:
            f.write(struct.pack('<HH', mask, mask))
    t0 = time.time()
    yield
    try:
        time.sleep(thold - time.time() + t0)
    except IOError:
        pass
    if line >= 0:
        with open('/proc/iguana/dio', 'wb') as f:
            f.write(struct.pack('<HH', 0, mask))


@click.command()
@click.option('--gate', type=int, default=-1,
              help='DIO line for GATE signal')
@click.option('--vmax', type=float, default=1, help='peak waveform voltage')
@click.option('--hwtrigger/--no-hwtrigger', default=False,
              help='use hardware trigger')
@click.option('--taper', type=float, default=0.1,
              help='waveform taper fraction')
@click.option('--f1', type=float, default=0,
              help='ending frequency for sweeps')
@click.option('--output', '-o', metavar='FILE',
              help='save waveform to FILE')
@click.option('--raw', metavar='FILE',
              help='save raw waveform values to FILE')
@click.argument('ncycles', type=int)
@click.argument('fs', type=int)
@click.argument('f0', type=int)
def cli(gate, vmax, hwtrigger, taper, f1, output, raw, ncycles, fs, f0):
    """
    Generate and transmit a waveform of length NCYCLES at frequency
    F0 through ARMS D/A board. The sampling frequency is FS.

    If '--f1' is given, the transmitted waveform will be an FM sweep
    with starting frequency F0 and ending frequency F1. In this case,
    NCYCLES will be measured at the average frequency.
    """
    if f1 > f0:
        p = fmslide(ncycles, fs, f0, f1, taper=taper)
    else:
        p = tone(ncycles, fs, f0, taper=taper)
    if output:
        np.savetxt(output, p, fmt='%.6f')
    ag = ArbGen(USBDA128A())
    ag.fsample = fs
    ag.vpeak = 2.5
    try:
        ag.add_waveform('sig1', vmax, p)
    except ValueError:
        click.secho('Waveform too large', fg='red')
        raise SystemExit(1)
    if raw:
        with open(raw, 'wb') as f:
            ag.waveforms['sig1'].dump(f)
    plen = len(p) / float(ag.fsample)
    quit = False
    while not quit:
        click.echo('q to quit, any other key to continue: ', nl=False)
        c = click.getchar()
        click.echo()
        if c not in ('q', 'Q'):
            click.secho('Uploading waveform ... ', fg='green', nl=False)
            try:
                ag.arm('sig1', timeout=4000, use_hardware=hwtrigger)
            except RuntimeError:
                click.secho('FAILED', fg='red')
            else:
                click.secho('done', fg='green')
                with amp_gate(gate, plen * 1.1):
                    ag.trigger()
        else:
            quit = True
