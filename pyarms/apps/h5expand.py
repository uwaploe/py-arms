#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from functools import partial
import click
import h5py
import numpy as np
try:
    import xattr
except ImportError:
    # Make setxattr a no-op
    def setxattr(*args, **kwds):
        pass
else:
    if hasattr(xattr, 'set'):
        setxattr = xattr.set
    else:
        setxattr = xattr.setxattr


def NS_USER(name):
    return 'user.' + name


def expand(name, obj, vscale=None, dgroup='pings'):
    """
    Expand an ARMS HDF5 object. Groups become directories and data-sets
    become files. If the :mod:`xattr` module is available, object
    attributes are stored as extended file attributes in the *user*
    namespace.

    :param name: object name (full path)
    :param obj: HDF5 Group or Dataset
    :param vscale: voltage scale factor. If not ``None``, this value will
                   be used to convert the A/D signal data-sets from counts
                   to volts.
    """
    parts = [str(s) for s in name.split('/')]
    if isinstance(obj, h5py.Group):
        if not os.path.exists(name):
            click.secho('Creating directory: {}'.format(name), fg='green')
            os.makedirs(name)
    elif isinstance(obj, h5py.Dataset):
        if obj.dtype.kind == 'f':
            fmt = '%.7f'
        elif obj.dtype.kind in ('S', 'U'):
            fmt = '%s'
        else:
            fmt = '%d'
        click.secho('Saving data-set: {}'.format(name), fg='green')
        if obj.shape == ():
            # Scalar values
            with open(name, 'wb') as f:
                f.write(fmt % obj[()] + '\n')
        else:
            if parts[0] == dgroup and parts[2] == 'signal':
                # Received data
                if vscale:
                    np.savetxt(name, obj[:, :]*vscale, fmt='%.7f')
                    setxattr(name, NS_USER('units'), 'volts')
                else:
                    np.savetxt(name, obj, fmt=fmt)
                    setxattr(name, NS_USER('units'), 'counts')
            elif parts[1] == 'xmtr' and parts[2] == 'signal':
                # Transmitted waveform
                np.savetxt(name, obj, fmt='%.4f')
            else:
                # Other N-arrays
                np.savetxt(name, obj, fmt=fmt)
            setxattr(name, NS_USER('dims'),
                     ','.join([str(s) for s in obj.shape]))
        setxattr(name, NS_USER('dtype'), obj.dtype.str)
    # Store HDF5 attributes as extended file attributes.
    for k, v in obj.attrs.iteritems():
        setxattr(name, NS_USER(k), repr(v))


@click.command()
@click.option('--dgroup', default='pings',
              help='name of HDF5 group with A/D data')
@click.option('--volts/--no-volts', default=False,
              help='write A/D values as volts rather than counts')
@click.argument('h5file', type=click.Path(exists=True))
@click.argument('outdir')
def cli(dgroup, volts, h5file, outdir):
    """
    Extract the contents of an HDF5 data file into a directory tree
    rooted at OUTDIR. Each group is a subdirectory and each data-set
    is a separate file.
    """
    if dgroup.startswith('/'):
        dgroup = dgroup[1:]

    f = h5py.File(h5file, 'r')
    if not os.path.exists(outdir):
        click.secho('Creating directory: {}'.format(outdir), fg='green')
        os.makedirs(outdir)
    # HDF5 file attributes get attached to the directory
    for k, v in f.attrs.iteritems():
        setxattr(outdir, NS_USER(k), repr(v))
    os.chdir(outdir)
    if volts:
        vscale = f['/metadata/rcvr/vscale'][()]
        f.visititems(partial(expand, vscale=vscale, dgroup=dgroup))
    else:
        f.visititems(expand, dgroup=dgroup)
