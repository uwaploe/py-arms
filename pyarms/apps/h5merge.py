#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import h5py
import os
import numpy as np
from contextlib import closing
from ast import literal_eval
import xattr


if hasattr(xattr, 'get_all'):
    get_all_xattr = xattr.get_all
else:
    def get_all_xattr(f):
        return [(k, v) for k, v in xattr.listxattr(f)
                if k.startswith('user.')]


def merge_pings(f, group, comp_type, clean):
    """
    Merge the contents of all ping files in the current directory
    into the corresponding group in the HDF5 file.
    """
    nchans = len(f['/metadata/rcvr/channels'])
    for name, grp in f[group].items():
        if 'signal' in grp:
            continue
        path = name + '.bin'
        if os.path.exists(path):
            click.secho('Adding {}'.format(name), fg='green')
            data = np.fromfile(path, dtype=np.int32)
            window = len(data) // nchans
            if window > 0:
                data.shape = (window, nchans)
                grp.create_dataset('signal',
                                   (window, nchans),
                                   data=data,
                                   compression=comp_type,
                                   dtype='i4').attrs['units'] = 'counts'
            else:
                click.secho('No data in {}'.format(path), fg='red')
            if clean:
                click.secho('Removing {}'.format(path), fg='green')
                os.unlink(path)
        else:
            click.secho('{} not found'.format(name), fg='red', err=True)


def quote(s):
    """Quote a string"""
    return '"' + s + '"'


def merge_metadata(f, clean, binary=True):
    """
    Merge the contents of the ./metadata directory tree into
    the /metadata group in the HDF5 file.
    """
    sep = '' if binary else '\n'
    for root, dirs, files in os.walk('metadata', topdown=False):
        grp_path = '/' + root
        if grp_path not in f:
            f.create_group(grp_path)
        grp = f[grp_path]
        for name in files:
            with open(os.path.join(root, name), 'rb') as infile:
                click.secho('Merging {}'.format(infile.name), fg='green')
                xa = dict(get_all_xattr(infile))
                if 'dims' in xa:
                    dims = tuple(xa['dims'].split(','))
                else:
                    dims = ()
                dtype = literal_eval(quote(xa.get('dtype', 'f4')))
                data = np.fromfile(infile, sep=sep,
                                   dtype=dtype)
                ds = grp.create_dataset(name, dims, data=data, dtype=dtype)
                # All other file attributes are stored as attributes of
                # the data-set.
                for k, v in xa.items():
                    if k not in ('dims', 'dtype'):
                        try:
                            ds.attrs[k] = literal_eval(v)
                        except ValueError:
                            ds.attrs[k] = literal_eval(quote(v))
                if clean:
                    os.unlink(infile.name)
        if clean:
            for name in dirs:
                dname = os.path.join(root, name)
                click.secho('Removing {}'.format(dname))
                os.rmdir(dname)
    if clean:
        os.rmdir('metadata')


@click.command()
@click.option('--compress/--no-compress', default=False,
              help='compress ping data')
@click.option('--clean/--no-clean', default=True,
              help='remove ping files after merging')
@click.option('--group', default='/pings',
              help='name of HDF5 group containing ping data')
@click.argument('h5file', type=click.Path(exists=True))
def cli(compress, clean, group, h5file):
    dirname = os.path.dirname(os.path.abspath(h5file))
    filename = os.path.basename(h5file)
    # Work from the data file directory.
    os.chdir(dirname)
    with closing(h5py.File(filename, 'r+')) as f:
        comp_type = 'gzip' if compress else None
        merge_pings(f, group, comp_type, clean)
        if os.path.isdir('metadata'):
            merge_metadata(f, clean, binary=False)
