#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cmd2
import shlex
import time
import sys
import signal
from ast import literal_eval
from functools import partial
import serial
import click
from pyarms.ros import Rotator


def parse_cmdline(line):
    """
    Parse a command line into a list of arguments and a dictionary of
    variable assignments, akin to the ``*args`` and ``**kwds`` arguments
    to Python functions.

    >>> args, kwds = parse_cmdline('x 42 foo=bar baz=1,2,3')
    >>> args
    ['x', 42]
    >>> d = kwds.items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar')]

    """
    def maybe_eval(s):
        try:
            return literal_eval(s)
        except ValueError:
            return s
    params = {}
    args = []
    for arg in shlex.split(line):
        try:
            name, val = arg.split('=')
            if ',' in val:
                params[name] = [maybe_eval(v) for v in val.split(',')]
            else:
                params[name] = maybe_eval(val)
        except ValueError:
            args.append(maybe_eval(arg))
    return args, params


class RotatorStall(Exception):
    pass


class RotatorTimeout(Exception):
    pass


def sig_handler(signum, *args):
    if signum != signal.SIGALRM:
        sys.exit(1)


def rotator_wait(rot, t_limit, interval=0.5, foutput=None):
    """
    Monitor the Rotator movement.
    """
    signal.signal(signal.SIGALRM, sig_handler)
    signal.setitimer(signal.ITIMER_REAL, interval, interval)
    try:
        while time.time() < t_limit:
            signal.pause()
            status = rot.flags
            if status & 2:
                raise RotatorStall
            if (status & 1) == 0:
                return rot.flags
            if foutput:
                foutput('angle={:.1f} status={:02x}'.format(rot.angle, status))
        raise RotatorTimeout('Timeout')
    finally:
        signal.setitimer(signal.ITIMER_REAL, 0)


class RosApp(cmd2.Cmd):
    """
    Simple command interpreter for testing the Rotator

    Commands (type help <command>)
    ===============================================
    moveto move where limits brake accel speed
    steps reset stepto (experimental)
    quit
    """
    prompt = 'ROS> '
    rot = None

    def _output(self, msg, color=None):
        if color:
            self.poutput(self.colorize(msg, color))
        else:
            self.poutput(msg)

    def do_where(self, line):
        """where: show current rotator position"""
        assert self.rot is not None
        self._output('{:.1f}'.format(self.rot.angle), 'green')

    def do_brake(self, line):
        """brake [value]: get/set the braking value"""
        assert self.rot is not None
        args, _ = parse_cmdline(line)
        if args:
            self.rot.brake = args[0]
        else:
            self._output('{:d}'.format(self.rot.brake), 'green')

    def do_accel(self, line):
        """accel [deg/s^2]: get/set the rotator acceleration value"""
        assert self.rot is not None
        args, _ = parse_cmdline(line)
        if args:
            self.rot.accel = args[0]
        else:
            self._output('{:.1f}'.format(self.rot.accel), 'green')

    def do_speed(self, line):
        """speed [deg/s]: get/set the default speed"""
        assert self.rot is not None
        args, _ = parse_cmdline(line)
        if args:
            self.rot.maxvel = args[0]
        else:
            self._output('{:.1f}'.format(self.rot.maxvel), 'green')

    def do_moveto(self, line):
        """moveto DEG [speed=DEG/s]: move to an absolute position"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        speed = params.get('speed', 0)
        try:
            if speed > 0:
                self.rot.maxvel, speed = speed, self.rot.maxvel
            t0 = time.time()
            dt = self.rot.moveto(args[0])
            if dt > 0:
                flags = rotator_wait(self.rot, t0 + dt*2.5,
                                     foutput=partial(self._output,
                                                     color='green'))
                self._output('Elapsed time: {:.1f}s ({:.1f}s)'.format(
                    time.time() - t0,
                    dt))
                if flags & 0x02:
                    self._output('Stall detected', color='red')
        except Exception as e:
            self._output(str(e), 'red')
        finally:
            if speed > 0:
                self.rot.maxvel = speed

    def do_move(self, line):
        """move DEG [speed=DEG/s]: move to a relative position"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        speed = params.get('speed', 0)
        try:
            if speed > 0:
                self.rot.maxvel, speed = speed, self.rot.maxvel
            t0 = time.time()
            dt = self.rot.move(args[0])
            if dt > 0:
                flags = rotator_wait(self.rot, t0 + dt*2.5,
                                     foutput=partial(self._output,
                                                     color='green'))
                self._output('Elapsed time: {:.1f}s ({:.1f}s)'.format(
                    time.time() - t0,
                    dt))
                if flags & 0x02:
                    self._output('Stall detected', color='red')
        except Exception as e:
            self._output(str(e), 'red')
        finally:
            if speed > 0:
                self.rot.maxvel = speed

    def do_stepto(self, line):
        """stepto DEG [speed=DEG/s]: step to an absolute position"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        speed = params.get('speed', 0)
        try:
            if speed == 0:
                speed = self.rot.maxvel
            t0 = time.time()
            dt, cmd = self.rot.stepto(args[0], speed)
            self._output('command = {!r}'.format(cmd))
            if dt > 0:
                flags = rotator_wait(self.rot, t0 + dt*2.5,
                                     foutput=partial(self._output,
                                                     color='green'))
                self._output('Elapsed time: {:.1f}s ({:.1f}s)'.format(
                    time.time() - t0,
                    dt))
                if flags & 0x02:
                    self._output('Stall detected', color='red')
        except Exception as e:
            self._output(str(e), 'red')

    def do_steps(self, line):
        """steps [DEG]: show the step counter or convert deg to steps"""
        assert self.rot is not None
        args, _ = parse_cmdline(line)
        if args:
            self._output(str(self.rot.steps(args[0])), 'green')
        else:
            self._output(str(self.rot.counter), 'green')

    def do_reset(self, line):
        """reset: reset the step counter"""
        assert self.rot is not None
        self.rot.reset_counter()

    def do_limits(self, line):
        """limits [ccw=DEG] [cw=DEG]: get/set user limits"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        ccw = params.get('ccw', -1)
        if ccw >= 0:
            self.rot.ccw = ccw
        cw = params.get('cw', -1)
        if cw >= 0:
            self.rot.cw = cw
        self._output('ccw={0.ccw:.0f} cw={0.cw:.0f}'.format(self.rot))

    def do_load(self, args):
        """load FILENAME: execute commands from a file."""
        if not args:
            self.perror('Missing filename\n')
        else:
            name = args.strip()
            colors, self.colors = self.colors, False
            try:
                with open(name, 'r') as f:
                    for line in f:
                        if not line.startswith('#'):
                            line = self.precmd(line.strip())
                            if line:
                                self.poutput('>' + line + '\n')
                                stop = self.onecmd(line)
                                if stop:
                                    return True
            except IOError as e:
                self.perror(str(e) + '\n')
            finally:
                self.colors = colors

    def do_help(self, arg):
        if arg:
            cmd2.Cmd.do_help(self, arg)
        else:
            self.poutput(self.__doc__ + '\n')

    def emptyline(self):
        pass


@click.command()
@click.option('-b', '--baud', default=9600, type=int,
              help='set serial baud rate')
@click.argument('device')
@click.argument('commands', nargs=-1)
def cli(baud, device, commands):
    """
    Interactive test program for an ROS Rotator. DEVICE is the serial
    port that the Rotator is connected to.
    """
    port = serial.Serial(device, baud, timeout=3)
    obj = RosApp()
    obj.rot = Rotator(port)
    obj.echo = False
    try:
        if not obj.run_commands_at_invocation(commands):
            obj._cmdloop()
    finally:
        if obj.rot and obj.rot.is_moving:
            obj.rot.stop()
